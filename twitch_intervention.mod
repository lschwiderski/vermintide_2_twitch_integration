return {
	run = function()
    fassert(rawget(_G, "new_mod"), "The mod \"Twitch Plus\" must be lower than Vermintide Mod Framework in your launcher's load order.")

    local mod_resources = {
      mod_script       = "scripts/mods/twitch_intervention/twitch_intervention",
      mod_data         = "scripts/mods/twitch_intervention/twitch_intervention_data",
      mod_localization = "scripts/mods/twitch_intervention/twitch_intervention_localization"
    }
    local mod = new_mod("twitch_intervention", mod_resources)
	end,
	packages = {
		"resource_packages/twitch_intervention/twitch_intervention"
	}
}
