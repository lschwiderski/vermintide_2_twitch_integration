--[[
  Copyright 2020 Lucas Schwiderski

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
--]]

local mod = get_mod("twitch_intervention")

local managers = mod:persistent_table("managers")

local buff_manager = managers.buff_manager
local twitch_manager = managers.twitch_manager

TwitchInterventionDebugger = class(TwitchInterventionDebugger)

function TwitchInterventionDebugger:init(world, is_server)
  self.world = world
  self.is_server = is_server

  self.screen_gui = World.create_screen_gui(world, "material", "materials/fonts/gw_fonts", "immediate")

  self.font_size = 26
  self.font_size_medium = 22
  self.font = "arial"
  self.font_mtrl = "materials/fonts/" .. self.font

  local width = 0.249
  self.width = width
  self.win_x = 1 - (width + 0.01)
  self.win_y = 0.01
  self.border_left = 0.0025

  self.text_height = 0.02
  self.row_height = 0.03

  self.layer_text = 3
  self.layer_bg = 2
end

function TwitchInterventionDebugger:update()
  local gui = self.screen_gui

  local font_size = self.font_size
  local font_size_medium = self.font_size_medium
  local font = self.font
  local font_mtrl = self.font_mtrl

  local res_x, res_y = Application.resolution()
  local text_height = self.text_height
  local width = self.width
  -- local height = 0.2
  local border_left = self.border_left
  local win_x = self.win_x
  local win_y = self.win_y
  local row = win_y
  local row_height = self.row_height

  local layer_text = self.layer_text

  local text_left = win_x + border_left

  local color_beige = Color(237, 237, 152)
  local color_red = Color(237, 137, 137)
  local color_green = Color(137, 237, 137)

  -- Title
  local nx = ScriptGUI.itext_next_xy(
    gui,
    res_x, res_y,
    "Twitch Intervention: ",
    font_mtrl, font_size, font,
    text_left, row + text_height,
    layer_text,
    color_beige
  )

  local activated = twitch_manager:is_activated()

  ScriptGUI.itext(
    gui,
    res_x, res_y,
    activated and "Active" or "Inactive",
    font_mtrl, font_size_medium, font,
    nx, row + text_height,
    layer_text,
    activated and color_green or color_red
  )

  if activated then
    row = twitch_manager:debug(self, res_x, res_y, row + row_height)
  end

  row = buff_manager:debug(self, res_x, res_y, row + (row_height * 2))

  row = row + row_height
  ScriptGUI.irect(gui, res_x, res_y, win_x, win_y, win_x + width, row, self.layer_bg, Color(100, 10, 10, 10))
end
