--[[
  Copyright 2020 Lucas Schwiderski

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
--]]

local mod = get_mod("twitch_intervention")

-- Keep root level references for efficiency
local managers = mod:persistent_table("managers")

local template_manager = managers.template_manager

-- The game mode as used in the vanilla game
TwitchInterventionVanillaGameMode = class(TwitchInterventionVanillaGameMode, TwitchInterventionBaseGameMode)

-- Min. number of vote templates that can be left in a rotation before used votes have to be cleared
-- 2 are needed for a successful `standard_vote`
local MIN_VOTES_LEFT_IN_ROTATION = 2

-- Number of votes until a template may be reused
local NUM_ROUNDS_TO_DISABLE_USED_VOTES = 15

function TwitchInterventionVanillaGameMode:init(twitch_manager, chat_connection)
  mod:debug("[TwitchInterventionVanillaGameMode:init] Called")
  TwitchInterventionVanillaGameMode.super.init(self, twitch_manager, chat_connection)

  self._timer = mod:get("twitch_initial_downtime")
  -- TODO: Move to mod options
  self._funds = TwitchSettings.starting_funds
  self._vote_keys = {}
  self._used_vote_templates = {}
end

function TwitchInterventionVanillaGameMode:destroy()
  TwitchInterventionVanillaGameMode.super.destroy(self)

  for vote_key in pairs(self._vote_keys) do
    self._parent:deregister_vote(vote_key)
  end
end

function TwitchInterventionVanillaGameMode:update(dt)
  if self._paused then
    return
  end

  self._timer = self._timer - dt

  -- TODO: What is the reason for checking `self._game` (which was taken over from vanilla code)?
  -- Is there a scenario where this might not be set, even though we are in an active mission?
  if self._timer <= 0 then
    mod:debug("[TwitchInterventionVanillaGameMode:update] Finding next vote with vanilla strategy")

    local vote_type, vote_templates = self:_get_next_vote()

    if vote_type == "standard_vote" then
      mod:debug("[TwitchInterventionVanillaGameMode:update] [%s] Vote 1: %s, Vote 2: %s", vote_type, vote_templates[1], vote_templates[2])
    else
      mod:debug("[TwitchInterventionVanillaGameMode:update] [%s] Vote: %s", vote_type, vote_templates[1])
    end

    local time = mod:get("twitch_vote_time")
    local vote_key = self._parent:register_vote(vote_type, vote_templates, time, callback(self, "cb_on_vote_complete"))
    if not vote_key then
      mod:error("[TwitchInterventionVanillaGameMode:update] Failed to register next vote")
      return
    end
    self._vote_keys[vote_key] = true

    self._timer = mod:get("twitch_time_between_votes") + time
  end
end

function TwitchInterventionVanillaGameMode:cb_on_vote_complete(current_vote)
  local template_name = current_vote.winning_template_name
  local winning_template = template_manager:get(template_name)

  self._funds = self._funds + winning_template.cost

  self._used_vote_templates[template_name] = NUM_ROUNDS_TO_DISABLE_USED_VOTES
  self._vote_keys[current_vote.vote_key] = nil
end

-- Vote choosing strategy: funds based
-- Every vote template has a certain `cost` which will be added to a `fund`. This `fund` can therefore roughly represent
-- the difficulty change by votes. If a lot of difficult votes (e.g. boss spawn) were chosen, the `fund` goes up.
-- Once the `fund` reaches a certain threshold in either direction, it is guaranteed that the next vote will have a cost
-- towards the other direction. Otherwise, it is chosen.
-- When a template got chosen, it will be added to a list of used templates which is only cleared when not enough unused
-- templates are availbale.
-- Whether a `multiple_choice` or `standard_vote` template is chosen is implicitly handled by the above.

function TwitchInterventionVanillaGameMode:_update_used_votes()
  local used_vote_templates = self._used_vote_templates

  for template_name, rounds_left in pairs(used_vote_templates) do
    if rounds_left - 1 == 0 then
      used_vote_templates[template_name] = nil
    else
      used_vote_templates[template_name] = rounds_left - 1
    end
  end

  self:_clear_used_votes()
end

function TwitchInterventionVanillaGameMode:_clear_used_votes(force_clear)
  local used_vote_templates = self._used_vote_templates
  local num_vote_templates = #template_manager:get_lookup("all")

  if force_clear or num_vote_templates - table.size(used_vote_templates) <= MIN_VOTES_LEFT_IN_ROTATION then
    table.clear(used_vote_templates)
  end
end

function TwitchInterventionVanillaGameMode:_get_next_vote()
  local forced_template = template_manager:get_forced()

  if forced_template then
    if forced_template.multiple_choice then
      return self:_next_multiple_choice_vote(forced_template)
    else
      return self:_next_standard_vote(forced_template)
    end
  end

  self:_update_used_votes()

  local funds = self._funds
  local used_vote_templates = self._used_vote_templates
  local best_template = nil

  -- TODO: Move setting to mod setting
  if TwitchSettings.cutoff_for_guaranteed_positive_vote <= funds then
    local templates = table.clone(template_manager:get_lookup("positive"))

    table.shuffle(templates)

    local best_diff = -math.huge

    for i = 1, #templates, 1 do
      local template_name = templates[i]

      if not used_vote_templates[template_name] then
        local template = template_manager:get(template_name)
        local is_allowed = not template.condition_func or template.condition_func()

        if is_allowed then
          local cost = template.cost
          local diff = funds - cost

          if best_diff < diff then
            best_template = template
            best_diff = diff
          end
        end
      end
    end
  -- TODO: Move setting to mod setting
  elseif funds <= TwitchSettings.cutoff_for_guaranteed_negative_vote then
    local templates = table.clone(template_manager:get_lookup("negative"))

    table.shuffle(templates)

    local best_diff = math.huge

    for i = 1, #templates, 1 do
      local template_name = templates[i]

      if not used_vote_templates[template_name] then
        local template = template_manager:get(template_name)
        local is_allowed = not template.condition_func or template.condition_func()

        if is_allowed then
          local cost = template.cost
          local diff = funds + cost

          if best_diff > diff then
            best_template = template
            best_diff = diff
          end
        end
      end
    end
  else
    local templates = table.clone(template_manager:get_lookup("all"))

    table.shuffle(templates)

    for i = 1, #templates, 1 do
      local template_name = templates[i]

      if not used_vote_templates[template_name] then
        local template = template_manager:get(template_name)
        local is_allowed = not template.condition_func or template.condition_func()

        if is_allowed then
          best_template = template
        end
      end
    end
  end

  if best_template.multiple_choice then
    return self:_next_multiple_choice_vote(best_template)
  else
    return self:_next_standard_vote(best_template)
  end
end

function TwitchInterventionVanillaGameMode:_next_standard_vote(template_a)
  local used_vote_templates = self._used_vote_templates
  local template_a_name = template_a.name
  local cost_a = template_a.cost
  local templates = table.clone(template_manager:get_lookup("standard"))

  table.shuffle(templates)

  local best_template = nil
  local best_diff = math.huge

  for i = 1, #templates, 1 do
    local template_b_name = templates[i]

    if template_a_name ~= template_b_name and not used_vote_templates[template_b_name] then
      local template_b = template_manager:get(template_b_name)
      local is_allowed = not template_b.condition_func or template_b.condition_func()

      if is_allowed then
        -- The game's twitch game mode actually has an additional check here, where, if `template_a` spawns a boss,
        -- `template_b` also has to spawn a boss or an equivalent threat.
        -- However that matching wasn't well implemented and restricting which templates can appear together
        -- limits the number of combinations, so we'll just remove it

        local cost_b = template_b.cost
        local vote_cost_diff = math.abs(cost_a - cost_b)

        -- TODO: Move to mod options, or at least a shared constant
        if vote_cost_diff <= TwitchSettings.max_a_b_vote_cost_diff and vote_cost_diff < best_diff then
          best_template = template_b
          best_diff = vote_cost_diff
        end
      end
    end
  end

  -- If we couldn't find any matching vote, then we probably don't have another standard vote left unused
  -- Clear the used votes and try again
  if not best_template then
    self:_clear_used_votes(true)

    return self:_next_standard_vote(template_a)
  end

  -- We don't need package chcking. On PC it should be fine without, as the `bosses` and `specials`
  -- breed categories (see `EnemyPackageLoaderSettings.categories`) are both set to `dynamic_loading = false`,
  -- which will make the game always load them up front.
  -- On console however, right now, there can only be one boss breed and three specials breeds loaded at a time.
  -- To stay safe, should check if that `dynamic_loading` stays at that value between patches.

  local vote_templates = {
    template_a.name,
    best_template.name
  }

  return "standard_vote", vote_templates
end

function TwitchInterventionVanillaGameMode:debug(debugger, res_x, res_y, row)
  local gui = debugger.screen_gui

  local font_size_medium = debugger.font_size_medium
  local font = debugger.font
  local font_mtrl = debugger.font_mtrl

  local border_left = debugger.border_left
  local win_x = debugger.win_x
  local text_height = debugger.text_height
  local row_height = debugger.row_height

  local layer_text = debugger.layer_text

  local text_left = win_x + border_left

  ScriptGUI.itext(
    gui,
    res_x, res_y,
    "Game Mode: [vanilla]",
    font_mtrl, font_size_medium, font,
    text_left, row + text_height,
    layer_text,
    debugger.color_white
  )

  row = row + row_height
  ScriptGUI.itext(
    gui,
    res_x, res_y,
    "Timer: " .. math.round_with_precision(self._timer, 2),
    font_mtrl, font_size_medium, font,
    text_left, row + text_height,
    layer_text,
    debugger.color_white
  )

  row = row + row_height
  ScriptGUI.itext(
    gui,
    res_x, res_y,
    "Funds: " .. tostring(self._funds),
    font_mtrl, font_size_medium, font,
    text_left, row + text_height,
    layer_text,
    debugger.color_white
  )

  return row
end
