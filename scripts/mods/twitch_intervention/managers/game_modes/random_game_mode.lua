--[[
  Copyright 2020 Lucas Schwiderski

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
--]]

local mod = get_mod("twitch_intervention")

-- Keep root level references for efficiency
local managers = mod:persistent_table("managers")

local template_manager = managers.template_manager

TwitchInterventionRandomGameMode = class(TwitchInterventionRandomGameMode, TwitchInterventionBaseGameMode)

function TwitchInterventionRandomGameMode:init(...)
  mod:debug("[TwitchInterventionRandomGameMode:init] Called")
  TwitchInterventionRandomGameMode.super.init(self, ...)

  self._timer = mod:get("twitch_initial_downtime")
  self._vote_keys = {}
end

function TwitchInterventionRandomGameMode:destroy()
  TwitchInterventionRandomGameMode.super.destroy(self)

  for vote_key in pairs(self._vote_keys) do
    self._parent:deregister_vote(vote_key)
  end
end

function TwitchInterventionRandomGameMode:update(dt)
  if self._paused then
    return
  end

  self._timer = self._timer - dt

  if self._timer <= 0 then
    mod:debug("[TwitchInterventionRandomGameMode:update] Finding next vote with random strategy")

    local vote_type, vote_templates = self:_get_next_vote()

    if vote_type == "standard_vote" then
      mod:debug("[TwitchInterventionRandomGameMode:update] [%s] Vote 1: %s, Vote 2: %s", vote_type, vote_templates[1], vote_templates[2])
    else
      mod:debug("[TwitchInterventionRandomGameMode:update] [%s] Vote: %s", vote_type, vote_templates[1])
    end

    local time = mod:get("twitch_vote_time")
    local vote_key = self._parent:register_vote(vote_type, vote_templates, time, callback(self, "cb_on_vote_complete"))
    if not vote_key then
      mod:error("[TwitchInterventionRandomGameMode:update] Failed to register next vote")
      return
    end
    self._vote_keys[vote_key] = true

    self._timer = mod:get("twitch_time_between_votes") + time
  end
end

function TwitchInterventionRandomGameMode:cb_on_vote_complete(current_vote)
  self._vote_keys[current_vote.vote_key] = nil
end

function TwitchInterventionRandomGameMode:_get_next_vote()
  -- Check for forced vote first
  local vote_template = template_manager:get_forced()

  if not vote_template then
    repeat
      -- Get random one until one is allowed
      -- TODO: Prevent infinite loop
      local template = template_manager:get_random()
      local is_allowed = not template.condition_func or template.condition_func()

      if is_allowed then
        vote_template = template
      end
    until vote_template
  end

  if vote_template.multiple_choice then
    return self:_next_multiple_choice_vote(vote_template)
  else
    return self:_next_standard_vote(vote_template)
  end
end

function TwitchInterventionRandomGameMode:_next_standard_vote(template_a)
  local template_b = template_a

  -- TODO: Prevent infinite loop
  repeat
    local template = template_manager:get_random("standard")
    local is_allowed = not template.condition_func or template.condition_func()

    if is_allowed then
      template_b = template
    end
  until template_b.name ~= template_a.name

  local vote_templates = {
    template_a.name,
    template_b.name
  }

  return "standard_vote", vote_templates
end

function TwitchInterventionRandomGameMode:debug(debugger, res_x, res_y, row)
  local gui = debugger.screen_gui

  local font_size_medium = debugger.font_size_medium
  local font = debugger.font
  local font_mtrl = debugger.font_mtrl

  local border_left = debugger.border_left
  local win_x = debugger.win_x
  local text_height = debugger.text_height

  local layer_text = debugger.layer_text

  local text_left = win_x + border_left

  ScriptGUI.itext(
    gui,
    res_x, res_y,
    "Game Mode: [random]",
    font_mtrl, font_size_medium, font,
    text_left, row + text_height,
    layer_text,
    debugger.color_white
  )

  return row
end
