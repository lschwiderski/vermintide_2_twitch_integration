--[[
  Copyright 2020 Lucas Schwiderski

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
--]]

local mod = get_mod("twitch_intervention")

-- The game mode is the system that handles the functionality needed during actual game play, i.e. during a mission
-- Essentially it handles vote timers and determines the next vote template.
-- It is initialized when the mission is loaded and destroyed when it finishes
-- This class serves as a base for the various possible game modes
TwitchInterventionBaseGameMode = class(TwitchInterventionBaseGameMode)


function TwitchInterventionBaseGameMode:init(twitch_manager)
  mod:debug("[TwitchInterventionBaseGameMode:init] Called")
  self._parent = twitch_manager
end

function TwitchInterventionBaseGameMode:destroy()
  if Managers.state and Managers.state.event then
    Managers.state.event:trigger("reset_vote_ui")
  end
end

-- Dummy method
function TwitchInterventionBaseGameMode:update(dt)
  return
end

function TwitchInterventionBaseGameMode:toggle_paused()
  self._paused = not self._paused
end

function TwitchInterventionBaseGameMode:_next_multiple_choice_vote(template)
  local vote_templates = {}

  for i = 1, 5, 1 do
    vote_templates[i] = template.name
  end

  if template.validation_func then
    mod:warning("[TwitchInterventionBaseGameMode] Validation functions for vote templates are currently not supported. Template: %s", template.name)
  end

  return "multiple_choice", vote_templates
end

function TwitchInterventionBaseGameMode:debug(debugger, res_x, res_y, row)
  local gui = debugger.screen_gui

  local font_size_medium = debugger.font_size_medium
  local font = debugger.font
  local font_mtrl = debugger.font_mtrl

  local border_left = debugger.border_left
  local win_x = debugger.win_x
  local text_height = debugger.text_height

  local layer_text = debugger.layer_text

  local text_left = win_x + border_left

  ScriptGUI.itext(
    gui,
    res_x, res_y,
    "Game Mode: [base]",
    font_mtrl, font_size_medium, font,
    text_left, row + text_height,
    layer_text,
    debugger.color_white
  )

  return row
end
