--[[
  Copyright 2020 Lucas Schwiderski

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
--]]

local mod = get_mod("twitch_intervention")

HookExtender = class(HookExtender)

function HookExtender:init()
  self._hooks = {}
  self._hook_chains = {}
end

function HookExtender:register_hook(object, func_name)
  if not (type(object) == "string" and type(func_name) == "string") then
    mod:error("Object and function name have to be of type string")
    return
  end

  local func_id = object .. "." .. func_name

  local hook_data = {
    initialized = false,
    enabled = false,
  }

  local hook_id = string.match(tostring(hook_data), "[a-z0-9A-Z]+", 7) .. "." .. func_name
  hook_data.id = hook_id

  self._hooks[hook_id] = hook_data

  local hook_chain = self._hook_chains[func_id]

  -- If the chain does not exist yet, the actual vmf hook doesn't exist either
  if not hook_chain then
    hook_chain = {}
    self._hook_chains[func_id] = hook_chain

    mod:hook(object, func_name, function(func, ...)
      return self:_create_chain(hook_chain, func)(...)
    end)
  end

  table.insert(hook_chain, hook_id)

  return hook_id
end

local function hook_chain_func_factory(func, hook_data)
  return function(...)
    return hook_data.hook_func(func, ...)
  end
end

function HookExtender:_create_chain(hook_chain, initial_func)
  if #hook_chain <= 0 then
    return initial_func
  end

  local func = initial_func
  for _, hook_id in ipairs(hook_chain) do
    repeat
      local hook_data = self._hooks[hook_id]
      if not hook_data then
        mod:error("Found chain entry for hook [%s] but no hook data", hook_id)
        break
      end

      if hook_data.enabled and hook_data.hook_func then
        func = hook_chain_func_factory(func, hook_data)
      end
    until true
  end

  return func
end

function HookExtender:init_hook(hook_id, hook_func)
  local hook_data = self._hooks[hook_id]
  if not hook_data then
    mod:error("Hook [%s] has not been registered", hook_id)
  else
    hook_data.hook_func = hook_func
    hook_data.initialized = true
  end
end

function HookExtender:enable_hook(hook_id)
  local hook_data = self._hooks[hook_id]
  if not hook_data then
    mod:error("Hook [%s] has not been registered", hook_id)
  elseif not hook_data.initialized then
    mod:error("Hook [%s] has not been initialized", hook_id)
  else
    hook_data.enabled = true
  end
end

function HookExtender:disable_hook(hook_id)
  local hook_data = self._hooks[hook_id]
  if hook_data then
    hook_data.enabled = false
  end
end
