--[[
  Copyright 2020 Lucas Schwiderski

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
--]]

local mod = get_mod("twitch_intervention")

local function verify_template(template)
  local errors = {}
  if not template or type(template) ~= "table" then
    table.insert(errors, "Data for template %s is not a table")
    return errors
  end

  if not template.cost or type(template.cost) ~= "number" then
    table.insert(errors, "Field 'texture_id' is not a string")
  end

  if not template.texture_id or type(template.texture_id) ~= "string" then
    table.insert(errors, "Field 'texture_id' is not a string")
  end

  if not template.text or type(template.text) ~= "string" then
    table.insert(errors, "Field 'texture_id' is not a string")
  end

  if not template.texture_size or type(template.texture_size) ~= "table" or #template.texture_size < 2 then
    table.insert(errors, "Field 'texture_size' is invalid")
  end

  if not template.on_success or type(template.on_success) ~= "function" then
    table.insert(errors, "Field 'texture_id' is not a string")
  end

  -- TODO: Verify table content
  return errors
end

TwitchTemplateManager = class(TwitchTemplateManager)

TwitchTemplateManager.init = function(self)
  self._templates = {}
  self._lookup = {
    all = {},
    standard = {},
    positive = {},
    negative = {},
    multiple_choice = {},
  }
  self._forced = {}
end

TwitchTemplateManager.reset = function(self)
  self._forced = {}
end

TwitchTemplateManager.add = function(self, name, template)
  if type(name) ~= "string" or name == "" then
    mod:error("[TwitchTemplateManager.add] Invalid vote template name '%s'.", name)
    return false
  end

  local errors = verify_template(template)
  if #errors > 0 then
    mod:warning("[TwitchTemplateManager.add] Template '%s' failed validation:", name)
    for _, err in ipairs(errors) do
      mod:warning("[TwitchTemplateManager.add] %s", err)
    end
    return false
  end

  -- Add dynamic values
  template.name = name

  if self._templates[name] then
    mod:error("[TwitchTemplateManager.add] Vote template '%s' already exists.", name)
    return false
  else
    local lookups = self._lookup
    self._templates[name] = template

    lookups.all[#lookups.all + 1] = name
    lookups.all[name] = #lookups.all

    if template.multiple_choice then
      lookups.multiple_choice[#lookups.multiple_choice + 1] = name
      lookups.multiple_choice[name] = #lookups.multiple_choice
    else
      lookups.standard[#lookups.standard + 1] = name
      lookups.standard[name] = #lookups.standard

      if template.cost < 0 then
        lookups.positive[#lookups.positive + 1] = name
        lookups.positive[name] = #lookups.positive
      else
        lookups.negative[#lookups.negative + 1] = name
        lookups.negative[name] = #lookups.negative
      end
    end
  end

  mod:debug("[TwitchTemplateManager.add] Template %s added", name)

  return true
end

TwitchTemplateManager.get_index = function(self, name)
  if type(name) ~= "string" or name == "" then
    mod:error("[TwitchTemplateManager.get_index] Invalid template name. Got value of type %s", type(name))
    return 0
  end

  return self._lookup.all[name] or 0
end

TwitchTemplateManager.get_by_index = function(self, index)
  if type(index) ~= "number" or index < 1 then
    mod:error("[TwitchTemplateManager.get_index] Invalid template index. Got [%s] %s", type(index), index)
    return nil
  end

  local template_name = self._lookup.all[index]

  if not template_name then
    mod:error("[TwitchTemplateManager.get_by_index] Could not get template for with index '%d'", index)
    return nil
  end

  return self._templates[template_name]
end

TwitchTemplateManager.get_all = function(self)
  return self._templates
end

TwitchTemplateManager.get_lookup = function(self, t)
  if t == "multiple_choice" then
    return self._lookup.multiple_choice
  elseif t == "standard" then
    return self._lookup.standard
  elseif t == "positive" then
    return self._lookup.positive
  elseif t == "negative" then
    return self._lookup.negative
  else
    return self._lookup.all
  end
end

TwitchTemplateManager.get = function(self, name)
  if type(name) == "number" then
    return self:get_by_index(name)
  end

  if type(name) ~= "string" or name == "" then
    mod:error("[TwitchTemplateManager.get] Invalid parameter for name: [%s] %s", type(name), name)
    return nil
  end

  local value = self._templates[name]

  if not value then
    mod:error("[TwitchTemplateManager.get] Could not get template for '%s'", name)
    return nil
  end

  return value
end

TwitchTemplateManager.get_random = function(self, t)
  local lookup
  if t == "all" or t == nil then
    lookup = self._lookup.all
  elseif t == "multiple_choice" then
    lookup = self._lookup.multiple_choice
  elseif t == "standard" then
    lookup = self._lookup.standard
  elseif t == "positive" then
    lookup = self._lookup.positive
  elseif t == "negative" then
    lookup = self._lookup.negative
  else
    mod:error("[TwitchTemplateManager.get_random] Unknown type of vote template: %s", t)
    return nil
  end

  local num_templates = #lookup
  if num_templates < 1 then
    mod:error("[TwitchTemplateManager.get_random] No templates registered.")
    return nil
  end

  local index = Math.random(num_templates)
  local name = lookup[index]

  return self:get(name)
end

TwitchTemplateManager.remove = function(self, name)
  self._templates[name] = nil
  return true
end

TwitchTemplateManager.get_forced = function(self)
  local list = self._forced
  local forced = list[1]

  if forced then
    table.remove(list, 1)
    return self:get(forced)
  end
end

TwitchTemplateManager.force = function(self, name)
  local template = self:get(name)

  if template then
    mod:debug("[TwitchTemplateManager.force] Forcing template %s", name)
    local list = self._forced
    list[#list + 1] = name
  else
    mod:debug("[TwitchTemplateManager.force] Template %s does not exist", name)
  end
end
