--[[
  Copyright 2020 Lucas Schwiderski

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
--]]

local mod = get_mod("twitch_intervention")

-- Keep root level references for efficiency
local managers = mod:persistent_table("managers")

local template_manager = managers.template_manager

-- Copy references to speed up slow access of global tables
local GameSession = GameSession
local TwitchSettings = TwitchSettings

local NOOP = function() end

local TWITCH_SOUND_BANK_PACKAGE = "resource_packages/ingame_sounds_twitch_mode"

local RPCS = {
  "rpc_update_twitch_vote"
}

local TWITCH_RPC_TYPES = {
  "rpc_finish_twitch_vote",
  "rpc_disconnected_from_twitch",
  "rpc_add_client_twitch_vote",

  rpc_finish_twitch_vote = 1,
  rpc_disconnected_from_twitch = 2,
  rpc_add_client_twitch_vote = 3,
}

-- TODO: Move to shared location
local TWITCH_VOTE_TYPE_LOOKUP = {
  "standard_vote",
  "multiple_choice",

  standard_vote = 1,
  multiple_choice = 2,
}

local function is_array(t)
  if not t then
    return false
  end

  for key, _ in pairs(t) do
    if type(key) ~= "number" then
      return false
    end
  end

  return true
end

local function key_to_string(key)
  local t = type(key)
  if t == "string" or t == "number" or t == "boolean" then
    return tostring(key)
  else
    return "[" .. t .. "]"
  end
end

-- Forward declareration, so the global ones won't be used instead
local value_to_string = nil;
local table_dump_string = nil;

local function array_dump_string(t, depth, max_depth)
  local str = "{\n"

  for i, value in ipairs(t) do
    str = str .. string.rep("\t", depth) .. value_to_string(value, depth, max_depth)

    if next(t, i) ~= nil then
      str = str .. ",\n"
    end
  end

  return str .. "\n" .. string.rep("\t", depth - 1) .. "}"
end

value_to_string = function(v, depth, max_depth)
  local t = type(v)
  depth = depth + 1

  if depth >= max_depth then
    return "[" .. t .. "]"
  end

  if t == "table" then
    if is_array(v) then
      return array_dump_string(v, depth, max_depth)
    elseif t == "table" then
      return table_dump_string(v, depth, max_depth)
    end
  elseif t == "string" then
    return "\"" .. v .. "\""
  elseif t == "boolean" or t == "function" or t == "number" or t == "nil" then
    return tostring(v)
  elseif t == "userdata" or t == "thread" then
    return "[" .. t .. "]"
  else
    return "[other]"
  end
end

table_dump_string = function(t, depth, max_depth)
  local str = "{\n"

  for key, value in pairs(t) do
    str = str .. string.rep("\t", depth) .. key_to_string(key) .. " = " .. value_to_string(value, depth, max_depth)

    if next(t, key) ~= nil then
      str = str .. ",\n"
    end
  end

  return str .. "\n" .. string.rep("\t", depth - 1) .. "}"
end

local function dump_string(t, max_depth)
  if not t then
    return tostring(t)
  elseif is_array(t) then
    return array_dump_string(t, 1, max_depth or 4)
  else
    return table_dump_string(t, 1, max_depth or 4)
  end
end

-- TwitchChatConnection establishes a connection to twitch's IRC chat. messages are parsed for votes by the viewers
-- TODO: Chat connections should be abstracted so that various interfaces can be used to gather votes
-- E.g. various chats of streaming platforms (twitch, mixer, youtube) but also other sources
TwitchChatConnection = class(TwitchChatConnection, BasicEventEmitter)

-- TwitchInterventionManager is the system that keeps the general functionality of the twitch mode
-- It is initialized on game boot and will not be destroyed until the game is closed
TwitchInterventionManager = class(TwitchInterventionManager)

function TwitchInterventionManager:init()
  mod:debug("[TwitchInterventionManager:init] Called")
  -- Map `vote_key` -> `vote_data`
  self._votes_lookup_table = {}

  -- Queue of registered votes that will be activated in order FIFO
  -- Right now each index is the atual `vote_data` object, which is somewhat redundant with `_votes_lookup_table`
  self._vote_queue = {}

  -- Base index used to generate consecutive, unique keys for registered votes
  self._vote_key_index = 1

  -- For each active vote a game object is registered to sync data with clients
  -- Since they are both numerical IDs, we need to separate lookups for each direction
  self._vote_key_to_go_id = {}
  self._go_id_to_vote_key = {}

  -- Initialize state
  self:_update_state()

  local twitch_username = mod:get("twitch_username")
  if mod:get("auto_connect") and twitch_username and string.len(twitch_username) > 0 then
    mod:debug("Attempting to connect to '%s'", twitch_username)
    self:connect(
      twitch_username,
      function()
        mod:error("Failed to connect automatically to Twitch user '%s'. Please connect manually through the mission selection screen.", twitch_username)
      end,
      function()
        mod:echo("Automatically connected to Twitch user '%s'", twitch_username)
      end
    )
  end
end

-- Free any initialized references
function TwitchInterventionManager:destroy()
  self:deactivate_twitch_game_mode()
  self:disconnect()

  self._network_manager = nil
  self._is_server = nil
  self._game = nil
end

-- Reset the twitch manager to it's default state. Semantically, as opposed to `destroy`,
-- this indicates that the same object reference will be re-used but should be reverted to a state
-- as if it was just initialized
function TwitchInterventionManager:reset()
  -- TODO: Keep certain data from the previous manager to re-initialize the new one
  self:destroy()
  self:init()
end

function TwitchInterventionManager:_update_state()
  -- Since certain references won't change between `update` ticks and are used by multiple methods,
  -- update only as needed and share
  local state_managers = Managers.state
  local network_manager = state_managers and state_managers.network
  self._network_manager = network_manager

  if network_manager then
    local is_server = network_manager.is_server
    self._is_server = is_server
    self._game = network_manager:game()

    local lobby = network_manager:lobby()
    if lobby then
      local twitch_enabled = lobby:lobby_data("twitch_enabled") == "true"

      if is_server then
        local is_connected = self:is_connected()
        local is_activated = self._activated

        if is_connected ~= is_activated then
          local lobby_data = lobby:get_stored_lobby_data()
          lobby_data.twitch_enabled = is_connected and "true" or "false"
          lobby:set_lobby_data(lobby_data)
          self._activated = is_connected
        elseif is_connected ~= twitch_enabled then
          local lobby_data = lobby:get_stored_lobby_data()
          lobby_data.twitch_enabled = is_connected and "true" or "false"
          lobby:set_lobby_data(lobby_data)
        end
      else
        self._activated = twitch_enabled
      end
    end
  else
    self._is_server = nil
  end
end

function TwitchInterventionManager:update(dt)
  self:_handle_disconnect_popup()

  self:_update_state()
  self:_update_vote_data(dt)

  if self._game_mode then
    self._game_mode:update(dt)
  end
end

function TwitchInterventionManager:_handle_disconnect_popup()
  local popup_id = self._error_popup_id
  if popup_id then
    local result = Managers.popup:query_result(popup_id)

    if result == "ok" then
      self._error_popup_id = nil
    elseif result then
      mod:error("[TwitchInterventionManager:_handle_disconnect_popup] Unexpected popup result for id %s: %s", popup_id, result)
    end
  end
end

function TwitchInterventionManager:_update_vote_data(dt)
  local is_server = self._is_server
  local game = self._game

  if is_server then
    if not self:is_activated() then
      return
    end

    local vote_key = self._current_vote

    if vote_key then
      local current_vote = self._votes_lookup_table[vote_key]
      current_vote.timer = current_vote.timer - dt

      self:_update_game_object(vote_key, current_vote)

      if current_vote.timer <= 0 then
        self:_execute_vote(current_vote)
        self:deregister_vote(vote_key)
      end
    end
  elseif game then
    -- On the client, iterate over each active vote and sync data with the respective game session object
    for vote_key, go_id in pairs(self._vote_key_to_go_id) do
      local vote_data = self:get_vote_data(vote_key)

      if vote_data then
        vote_data.options = GameSession.game_object_field(game, go_id, "options")
        vote_data.timer = GameSession.game_object_field(game, go_id, "time")
      end
    end
  end
end

function TwitchInterventionManager:_update_game_object(vote_key, vote_data)
  local game = self._game
  local go_id = self._vote_key_to_go_id[vote_key]

  if game and go_id then
    -- We can only sync integer values, but since we only show whole numbers in the UI anyways,
    -- the approximation is good enough
    local time = math.ceil(vote_data.timer)
    GameSession.set_game_object_field(game, go_id, "options", vote_data.options)
    GameSession.set_game_object_field(game, go_id, "time", math.max(time, 0))
  end
end

-- Register a new vote, add it to the queue and, if there is no active vote yet, activate
function TwitchInterventionManager:register_vote(vote_type, vote_templates, vote_time, on_vote_finish)
  mod:debug("[TwitchInterventionManager:register_vote] Registering new vote of type '%s'", vote_type)
  if not self:is_activated() then
    -- The most common cause will probably be that somehow the game mode is actiavted on a client, because it's mod is
    -- connected to a stream as well.
    mod:error(
      "[TwitchInterventionManager:register_vote] Votes can only be registered while the game mode is active. is_server: %s",
      self._is_server
    )
    return
  end

  -- Strings to display in vote for each option
  local default_strings = TwitchSettings[vote_type]
  local option_strings = {
    default_strings.default_vote_a_str,
    default_strings.default_vote_b_str,
    default_strings.default_vote_c_str,
    default_strings.default_vote_d_str,
    default_strings.default_vote_e_str,
  }

  -- To reduce the amount of validation needed down the line,
  -- ensure we always have a list of four (possibly valid) vote template indices.
  -- Might also be restricted by the network definition to always be this long
  local expanded_vote_templates = { 0, 0, 0, 0 }

  -- Now fill the actual templates for this vote
  for idx, vote_template in ipairs(vote_templates) do
    expanded_vote_templates[idx] = vote_template
  end

  -- TODO: Might not be a good idea to have a wrapping ID generator but nothing that ensures the IDs are unique
  -- Maybe network calls restrict the `vote_key` property to Byte size
  local vote_key = 1 + self._vote_key_index % 255

  local message_handler_id = self._chat_connection:on("message", callback(self, "cb_on_message_received", vote_key))

  local vote_data = {
    vote_key = vote_key,
    vote_type = vote_type,
    -- Not really in use, for now UI should always be shown
    show_vote_ui = true,
    activated = false,
    timer = vote_time,
    vote_templates = expanded_vote_templates,
    option_strings = option_strings,
    -- List to count user votes for each option
    options = { 0, 0, 0, 0, 0 },
    user_names = {},
    message_handler_id = message_handler_id,
    on_finish = on_vote_finish,
  }

  local queue_index = #self._vote_queue + 1

  -- Since `_activate_next_vote` is currently only ever called right after a vote is registered
  -- votes could potentially pile up in the queue without anything draining it.
  -- Log, in case  this leads to unexpected behavior
  if queue_index > 1 then
    mod:info("[TwitchInterventionManager:register_vote] Vote queue is growing at %d", queue_index)
  end

  self._vote_queue[queue_index] = vote_key
  self._votes_lookup_table[vote_key] = vote_data

  -- Update base index for the next iteration
  self._vote_key_index = vote_key

  -- TODO: Anything to check if the system is ready to start a new vote?
  if not self._current_vote then
    self:_activate_next_vote()
  end

  return vote_key
end

-- Register a new vote from a newly added game object. Mainly used by clients when the server sends the new vote
-- This is triggered by the server when it activates the vote, therefore the vote created here is already active as well
function TwitchInterventionManager:_register_networked_vote(go_id)
  local game = self._game
  local vote_key = GameSession.game_object_field(game, go_id, "vote_key")
  local vote_type = TWITCH_VOTE_TYPE_LOOKUP[GameSession.game_object_field(game, go_id, "vote_type")]

  -- Strings to display in vote for each option
  local default_strings = TwitchSettings[vote_type]
  local option_strings = {
    default_strings.default_vote_a_str,
    default_strings.default_vote_b_str,
    default_strings.default_vote_c_str,
    default_strings.default_vote_d_str,
    default_strings.default_vote_e_str,
  }

  -- Since vote_templates are networked as their indices, we need to get the actual data
  local networked_vote_templates = GameSession.game_object_field(game, go_id, "vote_templates")
  local vote_templates = {}

  for idx, template_index in ipairs(networked_vote_templates) do
    if template_index == 0 then
      mod:debug("[TwitchInterventionManager:_register_networked_vote] Got template index 0 at pos %s", idx)
      vote_templates[idx] = "none"
    else
      local template = template_manager:get(template_index)

      if template then
        vote_templates[idx] = template.name
        mod:debug(
          "[TwitchInterventionManager:_register_networked_vote] Got template '%s' for index %d at pos %s",
          template.name, template_index, idx
        )
      else
        vote_templates[idx] = "none"
        mod:error(
          "[TwitchInterventionManager:_register_networked_vote] Couldn't get a template for index %d at pos %s",
          template_index, idx
        )
      end
    end
  end

  local vote_data = {
    activated = true,
    timer = GameSession.game_object_field(game, go_id, "time"),
    option_strings = option_strings,
    options = GameSession.game_object_field(game, go_id, "options"),
    vote_templates = vote_templates,
    vote_key = vote_key,
    vote_type = vote_type,
    -- Not really in use, for now UI should always be shown
    show_vote_ui = true,
  }

  if self._chat_connection then
    vote_data.message_handler_id = self._chat_connection:on("message", callback(self, "cb_on_client_message_received", vote_key))
  end

  self._vote_queue[#self._vote_queue + 1] = vote_key
  self._votes_lookup_table[vote_key] = vote_data

  -- Since the vote is activated immediately, we also need to show the corresponding UI
  Managers.state.event:trigger("add_vote_ui", vote_key)

  mod:debug(
    "[TwitchInterventionManager:_register_networked_vote] Registered new vote '%s' of type '%s' at queue position %d:\n%s",
    vote_key, vote_type, #self._vote_queue, dump_string(vote_templates, 3)
  )

  return vote_key
end

function TwitchInterventionManager:_activate_next_vote()
  if self._current_vote then
    mod:error("[TwitchInterventionManager:_activate_next_vote] Can't activate next vote while there is still an active vote")
    return
  end

  local vote_key = self._vote_queue[1]
  local next_vote = self._votes_lookup_table[vote_key]

  -- If here was nothing in queue, there's nothing to do
  if not next_vote then
    mod:debug("[TwitchInterventionManager:_activate_next_vote] Queue is empty")
    return
  end

  next_vote.activated = true

  -- `show_vote_ui` is not really in use, for now UI should always be shown
  Managers.state.event:trigger("add_vote_ui", next_vote.vote_key)

  if not (self._is_server and self._game) then
    return
  end

  -- For the game session object, vote templates are represented by their ID/index
  local vote_template_indices = {}

  for idx, vote_template_name in ipairs(next_vote.vote_templates) do
    if vote_template_name == 0 or vote_template_name == "none" then
      -- The table will be padded to at least four values, even if we only need two values for a standard vote
      -- In that case the unused values `idx == 3` and `idx == 4` will always be `0`
      vote_template_indices[idx] = 0
    else
      local template_index = template_manager:get_index(vote_template_name)

      if template_index > 0 then
        mod:debug(
          "[TwitchInterventionManager:_activate_next_vote] Sending template '%s' as index %d at pos %s",
          vote_template_name, template_index, idx
        )
      else
        mod:error(
          "[TwitchInterventionManager:_activate_next_vote] Couldn't get a index for template '%s' at pos %s",
          vote_template_name, idx
        )
      end

      vote_template_indices[idx] = template_index
    end
  end

  local go_type = "twitch_vote"

  local game_object_fields = {
    go_type = NetworkLookup.go_types[go_type],
    vote_key = vote_key,
    options = next_vote.options,
    vote_type = TWITCH_VOTE_TYPE_LOOKUP[next_vote.vote_type],
    vote_templates = vote_template_indices,
    time = next_vote.timer,
    -- Not really in use, for now UI should always be shown
    show_vote_ui = true
  }

  -- The callback (3rd param) is required, be we don't actually need to do anything on session disconnect
  self._vote_key_to_go_id[vote_key] = self._network_manager:create_game_object(go_type, game_object_fields, NOOP)

  self._current_vote = vote_key
  table.remove(self._vote_queue, 1)
end

-- De-register a vote
function TwitchInterventionManager:deregister_vote(vote_key)
  mod:debug("[TwitchInterventionManager:deregister_vote] Deregistering vote '%s'", vote_key)

  local vote_data = self._votes_lookup_table[vote_key]

  if not vote_data then
    return
  end

  local message_handler_id = vote_data.message_handler_id
  if message_handler_id then
    self._chat_connection:off("message", message_handler_id)
  end

  if self._is_server then
    local go_id = self._vote_key_to_go_id[vote_key]

    if go_id then
      local game = self._game
      if game then
        GameSession.destroy_game_object(game, go_id)
      end

      self._vote_key_to_go_id[vote_key] = nil
      self._go_id_to_vote_key[go_id] = nil
    end

    -- If this was the currently active vote, ensure we get a new one instead
    if self._current_vote == vote_key then
      self._current_vote = nil
      self:_activate_next_vote()
    end
  end

  -- Remove from the queue
  for idx, queue_vote_key in ipairs(self._vote_queue) do
    if queue_vote_key == vote_key then
      table.remove(self._vote_queue, idx)
      break
    end
  end

  self._votes_lookup_table[vote_key] = nil
end

-- Connect to the chat service
function TwitchInterventionManager:connect(user_name)
  mod:debug("[TwitchInterventionManager:connect] Connecting to chat")

  if type(user_name) ~= "string" or user_name == "" then
    mod:error("[TwitchInterventionManager:connect] User name cannot be empty")
    return
  end

  -- TODO: Remove, so that everyone can contribute to voting
  if not self._is_server then
    mod:error("[TwitchInterventionManager:connect] Only the server may connect to twitch")
    return
  end

  if self._chat_connection then
    mod:debug("[TwitchInterventionManager:connect] Already connected")
    return
  end

  local chat_connection = TwitchChatConnection:new(user_name)

  chat_connection:on("disconnect", function()
    if self._network_manager and self._is_server then
      -- The game would complain if we didn't send any values for those parameters
      local unnecessary_int = 0
      local unnecessary_index = 0
      local unnecessary_lookup = 1
      local unnecessary_user_name = ""

      self._network_manager.network_transmit:send_rpc_clients(
        "rpc_update_twitch_vote",
        TWITCH_RPC_TYPES.rpc_disconnected_from_twitch,
        unnecessary_int,
        unnecessary_user_name,
        unnecessary_index,
        unnecessary_lookup
      )
    end

    if self._game_mode then
      self:deactivate_twitch_game_mode()

      self._error_popup_id = Managers.popup:queue_popup(
        Localize("popup_disconnected_from_twitch"),
        Localize("popup_header_error_twitch"),
        "return_to_inn",
        Localize("button_ok")
      )
    end
  end)

  chat_connection:on("connection_failure", function()
    chat_connection:destroy()
    self._chat_connection = nil
  end)

  -- TODO: When clients can connect to twitch as well, make this listener only on server, as that's the one who determines
  -- if twitch mode is actually active
  chat_connection:on("connection_success", function()
    local network_manager = self._network_manager
    local lobby = network_manager and network_manager:lobby()

    if lobby then
      lobby:set_lobby_data({ twitch_enabled = "true" })
    end
  end)

  self._chat_connection = chat_connection
end

function TwitchInterventionManager:disconnect()
  mod:debug("[TwitchInterventionManager:disconnect] Disconnecting")
  local connection_manager = self._chat_connection
  if not connection_manager then
    return
  end

  if self._game_mode then
    mod:error("[TwitchInterventionManager:disconnect] Can't disconnect from chat during an active mission")
    return
  end

  connection_manager:destroy()
  self._chat_connection = nil

  local network_manager = self._network_manager
  if network_manager then
    -- When this is called while the game shuts down, `network_transmit` has already been destroyed
    if network_manager.network_transmit then
      -- The game would complain if we didn't send any values for those parameters
      local unnecessary_int = 0
      local unnecessary_index = 0
      local unnecessary_lookup = 1
      local unnecessary_user_name = ""

      network_manager.network_transmit:send_rpc_clients(
        "rpc_update_twitch_vote",
        TWITCH_RPC_TYPES.rpc_disconnected_from_twitch,
        unnecessary_int,
        unnecessary_user_name,
        unnecessary_index,
        unnecessary_lookup
      )
    end

    -- If still activated, update lobby data
    if self._activated then
      local lobby = network_manager:lobby()
      if lobby then
        lobby:set_lobby_data({ twitch_enabled = "false" })
        mod:debug("[TwitchInterventionManager:disconnect] Set twitch_enabled = false")
      end
    end
  end

  self._activated = false
end

-- Returns whether the twitch manager is connected to a chat service
function TwitchInterventionManager:is_connected()
  return self._chat_connection and self._chat_connection:is_connected() or false
end

-- Returns whether the twitch manager is attempting to connect to a chat service
function TwitchInterventionManager:is_connecting()
  return self._chat_connection and self._chat_connection:is_connecting() or false
end

-- Returns the user's name to whose channel the manager is currently connected to
function TwitchInterventionManager:user_name()
  return self._chat_connection and self._chat_connection:user_name()
end

function TwitchInterventionManager:_get_new_game_mode()
  mod:debug("[TwitchInterventionManager:_get_new_game_mode] Called")
  local selected = mod:get("twitch_game_mode")

  if selected == "vanilla" then
    return TwitchInterventionVanillaGameMode:new(self, self._chat_connection)
  elseif selected == "random" then
    return TwitchInterventionRandomGameMode:new(self, self._chat_connection)
  else
    mod:error("Unknown game mode '%s'", selected)
  end
end

-- Start the twitch game mode, used when a mission loads
function TwitchInterventionManager:activate_twitch_game_mode(network_event_delegate, game_mode_key)
  if game_mode_key == "inn" then
    return
  end

  self._network_event_delegate = network_event_delegate
  network_event_delegate:register(self, unpack(RPCS))

  if not self._sound_bank_loaded and mod:get("twitch_enable_vote_sounds") then
    Managers.package:load(TWITCH_SOUND_BANK_PACKAGE, "twitch", nil, true)
    self._sound_bank_loaded = true
  end

  -- `activate_twitch_game_mode` is called before `update` could have picked up the new state of `is_server`
  -- so `self._is_server` is always `false` here
  local network_manager = Managers.state and Managers.state.network

  if network_manager.is_server and self:is_connected() then
    self._game_mode = self:_get_new_game_mode()
    mod:debug("[TwitchInterventionManager:activate_twitch_game_mode] Created game mode")
  end
end

-- Start the twitch game mode in debug mode. Will probably never happen in release build
function TwitchInterventionManager:debug_activate_twitch_game_mode()
  mod:debug("Starting twitch game mode in debug mode")
  self:activate_twitch_game_mode()
end

-- Stop the twitch game mode, used when a mission ends
function TwitchInterventionManager:deactivate_twitch_game_mode()
  if self._game_mode then
    self._game_mode:destroy()
    self._game_mode = nil
  end

  if self._network_event_delegate then
    self._network_event_delegate:unregister(self)
    self._network_event_delegate = nil
  end

  if self._sound_bank_loaded then
    Managers.package:unload(TWITCH_SOUND_BANK_PACKAGE, "twitch")
    self._sound_bank_loaded = false
  end
end

-- Returns whether the twitch mode is currently active
function TwitchInterventionManager:is_activated()
  return self._activated or false
end

-- Return the last error message, currently only used to display an error when the game disconnects from twitch mid mission
function TwitchInterventionManager:get_twitch_popup_message()
  local message = self._loading_popup_message
  self._loading_popup_message = nil

  return message
end

-- As a client, register the game session object identified by `go_id` as an active vote
function TwitchInterventionManager:add_game_object_id(go_id)
  local game = self._game

  if game then
    local vote_key = GameSession.game_object_field(game, go_id, "vote_key")
    self._vote_key_to_go_id[vote_key] = go_id
    self._go_id_to_vote_key[go_id] = vote_key

    self:_register_networked_vote(go_id)
  end
end

-- As a client, remove the registered vote associated with the game session object identified by `go_id`
function TwitchInterventionManager:remove_game_object_id(go_id)
  local game = self._game
  local vote_key = self._go_id_to_vote_key[go_id]

  if game and vote_key then
    self:deregister_vote(vote_key)

    self._vote_key_to_go_id[vote_key] = nil
    self._go_id_to_vote_key[go_id] = nil
  end
end

-- Get vote data for vote
function TwitchInterventionManager:get_vote_data(vote_key)
  return self._votes_lookup_table[vote_key]
end

-- Handle twitch vote rpc. This is actually just one RPC that uses the `twitch_rpc_type` parameter
-- to determine the actual function to call.
-- Parameters are:
--   - sender
--   - twitch_rpc_type
--   - vote_key
--   - user_name
--   - vote_index
--   - vote_template_id
function TwitchInterventionManager:rpc_update_twitch_vote(sender, twitch_rpc_type, ...)
  local rpc_type = TWITCH_RPC_TYPES[twitch_rpc_type]
  local rpc = self[rpc_type]

  if not rpc_type or not rpc then
    mod:warning("[TwitchInterventionManager:rpc_update_twitch_vote] Unknown RPC type '%s'", twitch_rpc_type)
    return
  end

  rpc(self, sender, ...)
end

function TwitchInterventionManager:rpc_finish_twitch_vote(_, vote_key, _, vote_index, vote_template_id)
  local vote_template = template_manager:get_by_index(vote_template_id)

  mod:debug(
    "[TwitchInterventionManager:rpc_finish_twitch_vote] Server triggered results for vote template '%s' with vote index %d",
    vote_template.name, vote_index
  )

  if vote_template then
    mod:pcall(function()
      vote_template.on_success(self._is_server, vote_index)
    end)
  end

  Managers.state.event:trigger("finish_vote_ui", vote_key, vote_index)

  self:deregister_vote(vote_key)
end

-- Clients may also connect to their own chat, which then contributes to the overall voting
function TwitchInterventionManager:rpc_add_client_twitch_vote(_, vote_key, user_name, vote_index)
  local vote_data = self._votes_lookup_table[vote_key]

  if not vote_data then
    mod:error("[TwitchInterventionManager:rpc_add_client_twitch_vote] Received voting for non-existent vote '%s'", vote_key)
    self:deregister_vote(vote_key)
    return
  elseif not vote_data.activated then
    mod:debug("[TwitchInterventionManager:rpc_add_client_twitch_vote] Received voting for inactive vote '%s'", vote_key)
    return
  end

  mod:debug(
    "[TwitchInterventionManager:rpc_add_client_twitch_vote] Received voting for vote '%s', option %d from client",
    vote_key, vote_index
  )

  if not mod:get("twitch_allow_multiple_votes") then
    if vote_data.user_names[user_name] then
      return
    end

    vote_data.user_names[user_name] = true
  end

  vote_data.options[vote_index] = vote_data.options[vote_index] + 1

  self:_update_game_object(vote_key, vote_data)
end

function TwitchInterventionManager:rpc_disconnected_from_twitch()
  self._loading_popup_message = "twitch_connection_failed"
end

function TwitchInterventionManager:_execute_vote(vote)
  local vote_type = vote.vote_type
  local vote_key = vote.vote_key
  local settings_options = TwitchSettings[vote_type]
  local num_options = table.size(settings_options)

  local best_option = -1
  local best_option_index = 0
  local options = vote.options

  local exclude_bots = mod:get("exclude_bot_players")
  local exclude_dead = mod:get("exclude_dead_players")

  -- Lookup to efficiently verify if the index from a multiple choice vote maps to a player
  local player_index_lookup = {}

  for _, player in pairs(Managers.player:human_and_bot_players()) do
    local profile_index = player:profile_index()
    player_index_lookup[profile_index] = player
  end

  -- Pick the winning option.
  for i = 1, num_options, 1 do
    repeat
      if vote_type == "multiple_choice" then
        local player = player_index_lookup[i]

        if not player then
          break
        end

        if exclude_bots and player.bot_player then
          break
        end

        if exclude_dead then
          local player_unit = player.player_unit
          -- When a player dies and hasn't respawned yet, they don't have a unit in the world
          if not player_unit then
            break
          end

          local status_extension = ScriptUnit.extension(player_unit, "status_system")
          if status_extension.ready_for_assisted_respawn or status_extension.assisted_respawning then
            break
          end
        end
      end

      local option = options[i]

      if best_option < option then
        best_option_index = i
        best_option = option
      elseif option == best_option then
        local random_winner = math.random(2)

        if random_winner == 1 then
          best_option_index = i
          best_option = option
        end
      end
    until true
  end

  local vote_template_name = vote.vote_templates[best_option_index]
  vote.winning_template_name = vote_template_name

  mod:debug(
    "[TwitchInterventionManager:_execute_vote] Executing winning vote template '%s' with vote_index %d",
    vote_template_name, best_option_index
  )

  mod:pcall(function()
    template_manager:get(vote_template_name).on_success(self._is_server, best_option_index)
  end)

  Managers.state.event:trigger("finish_vote_ui", vote_key, best_option_index)

  local unnecessary_user_name = ""

  self._network_manager.network_transmit:send_rpc_clients(
    "rpc_update_twitch_vote",
    TWITCH_RPC_TYPES.rpc_finish_twitch_vote,
    vote_key,
    unnecessary_user_name,
    best_option_index,
    template_manager:get_index(vote_template_name)
  )

  local on_finish = vote.on_finish
  if on_finish then
    on_finish(vote)
  end
end

function TwitchInterventionManager:cb_on_message_received(vote_key, _, user_name, message)
  -- Ignore direct conversation
  if string.find(message, "@") then
    return
  end

  local vote_data = self:get_vote_data(vote_key)

  if not vote_data then
    mod:error("[TwitchInterventionManager:cb_on_message_received] Received message for non-existent vote '%s'", vote_key)
    self:deregister_vote(vote_key)
    return
  elseif not vote_data.activated then
    mod:debug("[TwitchInterventionManager:cb_on_message_received] Received message for inactive vote '%s'", vote_key)
    return
  end

  message = string.lower(message)

  for idx, option_string in ipairs(vote_data.option_strings) do
    if string.find(message, option_string) then
      mod:debug("[TwitchInterventionManager:cb_on_message_received] Received message for vote '%s', option '%s'", vote_key, option_string)
      return self:vote_for_option(vote_key, user_name, idx)
    end
  end
end

function TwitchInterventionManager:cb_on_client_message_received(vote_key, user_name, message)
  -- Ignore direct conversation
  if string.find(message, "@") then
    return
  end

  local vote_data = self:get_vote_data(vote_key)

  if not vote_data then
    mod:error("[TwitchInterventionManager:cb_on_client_message_received] Received message for non-existent vote '%s'", vote_key)
    self:deregister_vote(vote_key)
    return
  elseif not vote_data.activated then
    mod:debug("[TwitchInterventionManager:cb_on_client_message_received] Received message for inactive vote '%s'", vote_key)
    return
  end

  message = string.lower(message)

  for idx, option_string in ipairs(vote_data.option_strings) do
    if string.find(message, option_string) then
      mod:debug("[TwitchInterventionManager:cb_on_client_message_received] Received message for vote '%s', option '%d'", vote_key, idx)
      return self:vote_for_option(vote_key, user_name, idx)
    end
  end
end

function TwitchInterventionManager:cb_connection_error_callback(...)
  return
end

function TwitchInterventionManager:cb_connection_success_callback(...)
  return
end

function TwitchInterventionManager:debug(debugger, res_x, res_y, row)
  local gui = debugger.screen_gui

  local font_size_medium = debugger.font_size_medium
  local font = debugger.font
  local font_mtrl = debugger.font_mtrl

  local border_left = debugger.border_left
  local win_x = debugger.win_x
  local text_height = debugger.text_height
  local row_height = debugger.row_height

  local layer_text = debugger.layer_text

  local text_left = win_x + border_left

  local color_white = Color(237, 237, 237)

  local connected = self:is_connected()

  if connected then
    ScriptGUI.itext(
      gui,
      res_x, res_y,
      "Connected as \"" .. self._chat_connection:user_name() .. "\"",
      font_mtrl, font_size_medium, font,
      text_left, row + text_height,
      layer_text,
      color_white
    )
  else
    ScriptGUI.itext(
      gui,
      res_x, res_y,
      "Peer connected",
      font_mtrl, font_size_medium, font,
      text_left, row + text_height,
      layer_text,
      color_white
    )
  end

  local game_mode = self._game_mode

  if game_mode then
    row = row + (row_height * 1.2)
    row = game_mode:debug(debugger, res_x, res_y, row)
  end

  local num_vote_queue = #self._vote_queue
  row = row + (row_height * 1.1)
  ScriptGUI.itext(
    gui,
    res_x, res_y,
    string.format("Vote Queue: %d %s", num_vote_queue, num_vote_queue == 1 and "entry" or "entries"),
    font_mtrl, font_size_medium, font,
    text_left, row + text_height,
    layer_text,
    color_white
  )

  row = row + row_height

  local current_vote = self._current_vote or self._vote_queue[1]

  if not current_vote then
    ScriptGUI.itext(
      gui,
      res_x, res_y,
      "No active vote",
      font_mtrl, font_size_medium, font,
      text_left, row + text_height,
      layer_text,
      color_white
    )
  else
    local vote_data = self:get_vote_data(current_vote)

    if vote_data.activated then
      ScriptGUI.itext(
        gui,
        res_x, res_y,
        "Active Vote: '" .. tostring(current_vote) .. "'",
        font_mtrl, font_size_medium, font,
        text_left, row + text_height,
        layer_text,
        color_white
      )

      row = row + (row_height * 0.8)
      ScriptGUI.itext(
        gui,
        res_x, res_y,
        "Timer: " .. math.round_with_precision(vote_data.timer, 2),
        font_mtrl, font_size_medium, font,
        text_left + 0.01, row + text_height,
        layer_text,
        color_white
      )

      local vote_type = vote_data.vote_type

      row = row + (row_height * 0.8)
      ScriptGUI.itext(
        gui,
        res_x, res_y,
        "Type: " .. tostring(vote_type),
        font_mtrl, font_size_medium, font,
        text_left + 0.01, row + text_height,
        layer_text,
        color_white
      )

      local vote_templates = vote_data.vote_templates

      row = row + (row_height * 0.8)
      if vote_type == "multiple_choice" then
        local template_name = vote_templates[1]
        local template_index = template_manager:get_index(template_name)

        ScriptGUI.itext(
          gui,
          res_x, res_y,
          string.format("Template: %s (%d)", template_name, template_index),
          font_mtrl, font_size_medium, font,
          text_left + 0.01, row + text_height,
          layer_text,
          color_white
        )
      else
        ScriptGUI.itext(
          gui,
          res_x, res_y,
          "Templates:",
          font_mtrl, font_size_medium, font,
          text_left + 0.01, row + text_height,
          layer_text,
          color_white
        )

        local template_a_name = vote_templates[1]
        local template_a_index = template_manager:get_index(template_a_name)

        row = row + (row_height * 0.8)
        ScriptGUI.itext(
          gui,
          res_x, res_y,
          "[" .. template_a_index .. "]: " .. template_a_name,
          font_mtrl, font_size_medium, font,
          text_left + 0.02, row + text_height,
          layer_text,
          color_white
        )

        local template_b_name = vote_templates[2]
        local template_b_index = template_manager:get_index(template_b_name)

        row = row + (row_height * 0.8)
        ScriptGUI.itext(
          gui,
          res_x, res_y,
          "[" .. template_b_index .. "]: " .. template_b_name,
          font_mtrl, font_size_medium, font,
          text_left + 0.02, row + text_height,
          layer_text,
          color_white
        )
      end

      row = row + (row_height * 0.8)
      ScriptGUI.itext(
        gui,
        res_x, res_y,
        "Options:",
        font_mtrl, font_size_medium, font,
        text_left + 0.01, row + text_height,
        layer_text,
        color_white
      )

      local option_strings = vote_data.option_strings
      for idx, option in ipairs(vote_data.options) do
        local name = option_strings[idx]

        if name then
          row = row + (row_height * 0.8)
          ScriptGUI.itext(
            gui,
            res_x, res_y,
            name .. ": " .. option,
            font_mtrl, font_size_medium, font,
            text_left + 0.02, row + text_height,
            layer_text,
            color_white
          )
        end
      end
    end
  end

  return row
end

function TwitchInterventionManager:vote_for_option(vote_key, user_name, option_index)
  -- As a client, send the RPC so the server handles this and therefore applies it's settings correctly
  if not self._is_server then
    local unnecessary_lookup = 1

    self._network_manager.network_transmit:send_rpc_server(
      "rpc_update_twitch_vote",
      TWITCH_RPC_TYPES.rpc_add_client_twitch_vote,
      vote_key,
      user_name,
      option_index,
      unnecessary_lookup
    )
    return
  end

  local vote_data = self:get_vote_data(vote_key)

  if not vote_data then
    mod:error("[TwitchInterventionManager:vote_for_option] Received message for non-existent vote '%s'", vote_key)
    self:deregister_vote(vote_key)
    return
  elseif not vote_data.activated then
    mod:error("[TwitchInterventionManager:vote_for_option] Received message for inactive vote '%s'", vote_key)
    return
  end

  local multiple_votes_allowed = mod:get("twitch_allow_multiple_votes")

  if not multiple_votes_allowed and vote_data.user_names[user_name] then
    return
  end

  local options = vote_data.options
  options[option_index] = options[option_index] + 1

  if not multiple_votes_allowed then
    vote_data.user_names[user_name] = true
  end

  -- TODO: Is this needed? It's called on every tick anyways, so it might be redundant to do here as well
  self:_update_game_object(vote_key, vote_data)
end

function TwitchInterventionManager:toggle_paused()
  if self._game_mode then
    self._game_mode:toggle_paused()
  end
end

function TwitchInterventionManager:game_mode_supported(game_mode)
  return true
end

----------------------------------------------
----------------------------------------------
----------------------------------------------

function TwitchChatConnection:init(user_name)
  mod:debug("[TwitchChatConnection:init] Called")
  TwitchChatConnection.super.init(self)

  Managers.irc:register_message_callback("twitch_chat", Irc.CHANNEL_MSG, function(_, ...)
    self:emit("message", ...)
  end)

  local settings = {
    address = "irc.chat.twitch.tv",
    port = 6667,
    allow_send = false,
    channel_name = "#" .. string.lower(string.gsub(user_name, ' ', '_'))
  }

  Managers.irc:connect(nil, nil, settings, function(is_connected)
    mod:debug("[TwitchChatConnection:init] IRC connection status changed. Connected: %s", is_connected)

    self._connected = is_connected

    -- If we already finished initial connection but are not connected now, we were disconnected
    if not self._connecting and not is_connected then
      self:emit("disconnect")
      return
    end

    self._connecting = false

    if is_connected then
      self:emit("connection_success")
    else
      self:emit("connection_failure")
    end
  end)

  self._connected = false
  self._connecting = true
  self._twitch_user = user_name
end

function TwitchChatConnection:destroy()
  -- During game shutdown the IRC manager destroyed before twitch
  if Managers.irc then
    Managers.irc:force_disconnect()
  end
end

function TwitchChatConnection:is_connected()
  return self._connected
end

function TwitchChatConnection:is_connecting()
  return self._connecting
end

function TwitchChatConnection:user_name()
  return self._twitch_user
end
