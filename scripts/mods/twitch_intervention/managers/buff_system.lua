--[[
  Copyright 2020 Lucas Schwiderski

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
--]]

local mod = get_mod("twitch_intervention")

TwitchBuffSystem = class(TwitchBuffSystem)

function TwitchBuffSystem:init()
  self._buffs = {}
  self._num_buffs = 0
  self._timers = {}
end

function TwitchBuffSystem:destroy()
  self:_cancel_all()
end

function TwitchBuffSystem:reset()
  self:_cancel_all()
end

function TwitchBuffSystem:update(dt)
  for buff_name, buff_timers in pairs(self._timers) do
    local buff = self._buffs[buff_name]
    for instance_id, timer in ipairs(buff_timers) do
      timer.time = timer.time + dt

      if timer.time > timer.duration then
        if buff.finish then
          mod:pcall(function() buff.finish(timer.state) end)
        end

        table.remove(buff_timers, instance_id)
      elseif buff.update and (not timer.tick_rate or (timer.procs * timer.tick_rate) <= timer.time) then
        timer.procs = timer.procs + 1

        local success, return_values = mod:pcall(function()
          return buff.update(timer.state, dt, timer.time)
        end)

        if not success and buff.cancel_on_error then
          self:cancel_buff(buff_name, instance_id)
          return
        end

        if return_values then
          timer.state = return_values[1]
        end
      end

      timer.last_tick = timer.time
    end
  end
end

function TwitchBuffSystem:on_game_state_changed(status)
  if status == "exit" then
    self:_cancel_all()
  end
end

function TwitchBuffSystem:has_buff(buff_name)
  return self._buffs[buff_name] ~= nil
end

--[[
  Expects a table:
  - name                    [string]    Name/Identifier of this buff
  - duration                [number]    Duration for the buff
  - tick_rate               [number]    Seconds between each call to `update`.                     (optional)
                                        If zero or nil, `update` will be called for each frame
  - start                   [function]  Function to execute when the buff begins                   (optional)
  - update                  [function]  Function to execute every frame                            (optional)
  - finish                  [function]  Function to execute when the buff ends                     (optional)
  - cancel                  [function]  Function to execute when the buff is canceled prematurely  (optional)
  - cancel_on_state_change  [bool]      Unless this is set to false, buff will be canceled         (optional)
                                        on game state change
  - single_instance         [bool]      Only one active instance of this buff is allowed at a time (optional)
--]]
function TwitchBuffSystem:register_buff(buff)
  local buff_name = buff.name
  if self._buffs[buff_name] then
    mod:error("[TwitchBuffSystem.register_buff] Buff %s already exists", buff_name)
    return
  end

  self._buffs[buff_name] = table.shallow_copy(buff)
  self._timers[buff_name] = {}
  self._num_buffs = self._num_buffs + 1
  mod:debug("[TwitchBuffSystem.register_buff] Registered buff %s", buff_name)
end

function TwitchBuffSystem:deregister_buff(buff_name)
  for id in ipairs(self._timers[buff_name]) do
    self:cancel_buff(buff_name, id)
  end
  self._buffs[buff_name] = nil
  self._timers[buff_name] = nil
  mod:debug("[TwitchBuffSystem.update] De-registered buff %s", buff_name)
end

function TwitchBuffSystem:start_buff(buff_name, options, ...)
  local buff = self._buffs[buff_name]
  if not buff then
    mod:error("[TwitchBuffSystem.start_buff] Buff with name '%s' does not exist", buff_name)
    return
  end

  options = options or {}
  local timer_id = options.timer_id or math.random()

  local timers = self._timers[buff_name] or {}
  if buff.single_instance then
    for instance_id in ipairs(timers) do
      self:cancel_buff(buff_name, instance_id)
    end
  end

  -- Stop all timers with the same timer ID
  for instance_id, timer in ipairs(timers) do
    if timer.timer_id == timer_id then
      mod:debug("[TwitchBuffSystem.start_buff] Timer (%d) with timer id %s already exists", instance_id, timer_id)
      self:cancel_buff(buff_name, instance_id)
    end
  end

  local initial_state = { args = {...} }
  if buff.start then
    local success, return_values = mod:pcall(function()
      return buff.start(initial_state)
    end)

    -- If start errored, return immediately to stop the buff
    if not success then
      return
    end

    if return_values then
      table.merge(initial_state, return_values[1])
    end
  end

  local instace_id = #timers + 1
  local timer = {
    buff = buff,
    state = initial_state,
    duration = options.duration or buff.duration,
    tick_rate = options.tick_rate or buff.tick_rate,
    procs = 0,
    time = 0,
    last_tick = 0,
    instace_id = instace_id,
    timer_id = timer_id,
  }

  if type(timer.duration) ~= "number" then
    mod:error("Can't register buff '%s' without a duration", buff_name)
    return
  end

  timers[instace_id] = timer
  self._timers[buff_name] = timers

  return instace_id
end

function TwitchBuffSystem:cancel_buff(buff_name, instance_id, ...)
  local timers = self._timers[buff_name]
  local timer = timers[instance_id]

  -- Nothing to cancel
  if not timer then
    mod:error("Buff %s or instance %d of this buff does not exist", buff_name, instance_id or -1)
    return
  end

  local buff = timer.buff
  if buff.cancel == "finish" then
    mod:pcall(function() buff.finish(timer.state) end)
  elseif buff.cancel then
    mod:pcall(function(...) buff.cancel(timer.state, ...) end, ...)
  end

  table.remove(timers, instance_id)
end

function TwitchBuffSystem:_cancel_all()
  for buff_name, buff_timers in pairs(self._timers) do
    for instance_id in ipairs(buff_timers) do
      self:cancel_buff(buff_name, instance_id)
    end
  end
end

function TwitchBuffSystem:debug(debugger, res_x, res_y, row)
  local gui = debugger.screen_gui

  local font_size = debugger.font_size
  local font_size_medium = debugger.font_size_medium
  local font = debugger.font
  local font_mtrl = debugger.font_mtrl

  local border_left = debugger.border_left
  local win_x = debugger.win_x
  local text_height = debugger.text_height
  local row_height = debugger.row_height

  local layer_text = debugger.layer_text

  local text_left = win_x + border_left

  local color_white = Color(237, 237, 237)
  local color_beige = Color(237, 237, 152)

  ScriptGUI.itext(
    gui,
    res_x, res_y,
    "Buff Manager: ",
    font_mtrl, font_size, font,
    text_left, row + text_height,
    layer_text,
    color_beige
  )

  row = row + row_height
  ScriptGUI.itext(
    gui,
    res_x, res_y,
    self._num_buffs .. " buffs registered.",
    font_mtrl, font_size_medium, font,
    text_left, row + text_height,
    layer_text,
    color_white
  )

  row = row + row_height
  local nx = ScriptGUI.itext_next_xy(
    gui,
    res_x, res_y,
    "Timers:",
    font_mtrl, font_size_medium, font,
    text_left, row + text_height,
    layer_text,
    color_white
  )

  local has_timer = 0

  for buff_name, buff_timers in pairs(self._timers) do
    row = row + (row_height * 0.8)
    local nx = ScriptGUI.itext_next_xy(
      gui,
      res_x, res_y,
      buff_name .. ": ",
      font_mtrl, font_size_medium, font,
      text_left + 0.01, row + text_height,
      layer_text,
      color_white
    )

    for instance_id, timer in ipairs(buff_timers) do
      local str = string.format(
        "[%s]: %s, %s",
        instance_id, timer.buff.update and (timer.procs .. " procs") or "", math.round_with_precision(timer.duration - timer.time, 2)
      )

      row = row + (row_height * 0.8)
      ScriptGUI.itext(
        gui,
        res_x, res_y,
        str,
        font_mtrl, font_size_medium, font,
        text_left + 0.02, row + text_height,
        layer_text,
        color_white
      )

      has_timer = 2
    end

    if has_timer < 2 then
      ScriptGUI.itext(
        gui,
        res_x, res_y,
        "none",
        font_mtrl, font_size_medium, font,
        nx, row + text_height,
        layer_text,
        color_white
      )
    end
    has_timer = 1
  end

  if has_timer < 1 then
    ScriptGUI.itext(
      gui,
      res_x, res_y,
      "none",
      font_mtrl, font_size_medium, font,
      nx, row + text_height,
      layer_text,
      color_white
    )
  end

  return row
end
