--[[
  Copyright 2020 Lucas Schwiderski

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
--]]

local mod = get_mod("twitch_intervention")

BasicEventEmitter = class(BasicEventEmitter)

function BasicEventEmitter:init()
  self._event_handlers = {}
end

function BasicEventEmitter:on(event_name, cb)
  local handlers = self._event_handlers[event_name] or {}
  table.insert(handlers, cb)
  self._event_handlers[event_name] = handlers
  mod:debug("[BasicEventEmitter:on] New handler for event '%s' registered", event_name)
  return #handlers
end

function BasicEventEmitter:off(event_name, cb_index)
  if not self._event_handlers[event_name] then
    mod:warning("[BasicEventEmitter:off] No handlers for event '%s' registered", event_name)
    return
  end

  table.remove(self._event_handlers[event_name], cb_index)
  mod:debug("[BasicEventEmitter:off] Handler %d for event '%s' removed", cb_index, event_name)
end

function BasicEventEmitter:emit(event_name, ...)
  mod:debug("[BasicEventEmitter:emit] Emitting event '%s'", event_name)

  local handlers = self._event_handlers[event_name]

  if not handlers then
    mod:debug("[BasicEventEmitter:emit] No listeners for event '%s'", event_name)
    return
  end

  for _, cb in ipairs(handlers) do
    cb(...)
  end
end
