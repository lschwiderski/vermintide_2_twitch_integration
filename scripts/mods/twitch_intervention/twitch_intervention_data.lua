--[[
  Copyright 2020 Lucas Schwiderski

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
--]]
local mod = get_mod("twitch_intervention")

local mod_data = {
  name = "Twitch Plus",
  description = "mod_description",
  is_togglable = true,
  -- Not actually a mutator in the sense of VT1, but affects gameplay
  -- Not usable right now
  -- is_mutator = true,
}

mod.SETTING_NAMES = {
  TEMPLATE_STRATEGY = "template_strategy",
  RANDOMIZE_TEMPLATE_OUTCOME = "randomize_template_outcome",
  RANDOMIZE_BOSS_TEMPLATE_OUTCOME = "randomize_boss_template_outcome",
  RANDOMIZATION_MULTIPLIER = "randomize_template_multiplier",
}

mod_data.options = {
  widgets = {
    {
      setting_id = "vote_time_options_group",
      type = "group",
      title = "vote_time_options_group_name",
      sub_widgets = {
        {
          setting_id = "twitch_time_between_votes",
          type = "numeric",
          title = "option_twitch_time_between_votes_name",
          tooltip = "option_twitch_time_between_votes_tooltip",
          default_value = 30,
          range = { 0, 150 },
        },
        {
          setting_id = "twitch_vote_time",
          type = "numeric",
          title = "option_twitch_vote_time_name",
          tooltip = "option_twitch_vote_time_tooltip",
          default_value = 30,
          range = { 0, 150 },
        },
        {
          setting_id = "twitch_initial_downtime",
          type = "numeric",
          title = "option_twitch_initial_downtime_name",
          tooltip = "option_twitch_initial_downtime_tooltip",
          default_value = 60,
          range = { 0, 120 },
        },
      },
    },
    {
      setting_id = "twitch_enable_vote_sounds",
      type = "checkbox",
      title = "option_twitch_enable_vote_sounds_name",
      tooltip = "option_twitch_enable_vote_sounds_tooltip",
      default_value = true,
    },
    {
      setting_id = "twitch_game_mode",
      type = "dropdown",
      title = "option_twitch_game_mode_name",
      tooltip = "option_twitch_game_mode_tooltip",
      default_value = "vanilla",
      options = {
        { text = "option_twitch_game_mode_option_vanilla", value = "vanilla" },
        { text = "option_twitch_game_mode_option_random", value = "random" },
      },
    },
    {
      setting_id = "twitch_allow_multiple_votes",
      type = "checkbox",
      title = "option_twitch_allow_multiple_votes_name",
      tooltip = "option_twitch_allow_multiple_votes_tooltip",
      default_value = false,
    },
    {
      setting_id = "twitch_allow_mutators",
      type = "checkbox",
      title = "option_twitch_allow_mutators_name",
      tooltip = "option_twitch_allow_mutators_tooltip",
      default_value = true,
      sub_widgets = {
        {
          setting_id = "twitch_mutators_duration_multiplier",
          type = "numeric",
          title = "option_twitch_mutators_duration_multiplier_name",
          tooltip = "option_twitch_mutators_duration_multiplier_tooltip",
          range = { -5, 10 },
          decimals_number = 1,
          default_value = 1,
        }
      }
    },
    {
      setting_id = "twitch_allow_positive_votes",
      type = "checkbox",
      title = "option_twitch_allow_positive_votes_name",
      tooltip = "option_twitch_allow_positive_votes_tooltip",
      default_value = true,
    },
    {
      setting_id = "twitch_allow_limited_time_votes",
      type = "checkbox",
      title = "option_twitch_allow_limited_time_votes_name",
      tooltip = "option_twitch_allow_limited_time_votes_tooltip",
      default_value = true,
    },
    {
      setting_id = "exclude_bot_players",
      type = "checkbox",
      title = "option_exclude_bot_players_name",
      tooltip = "option_exclude_bot_players_tooltip",
      default_value = false,
    },
    {
      setting_id = "exclude_dead_players",
      type = "checkbox",
      title = "option_exclude_dead_players_name",
      tooltip = "option_exclude_dead_players_tooltip",
      default_value = false,
    },
    {
      setting_id = "randomize_template_outcome",
      type = "checkbox",
      title = "option_randomize_template_outcome_name",
      tooltip = "option_randomize_template_outcome_tooltip",
      default_value = false,
      sub_widgets = {
        {
          setting_id = "randomize_boss_template_outcome",
          type = "checkbox",
          title = "option_randomize_boss_template_outcome_name",
          default_value = false,
        },
        {
          setting_id = "randomize_template_multiplier",
          type = "numeric",
          title = "option_randomize_template_multiplier_name",
          tooltip = "option_randomize_template_multiplier_tooltip",
          range = { 1, 20 },
          decimals_number = 1,
          default_value = 1,
        }
      }
    },
    {
      setting_id = "twitch_enable_plaza",
      type = "checkbox",
      title = "option_twitch_enable_plaza_name",
      tooltip = "option_twitch_enable_plaza_tooltip",
      default_value = true,
    },
    {
      setting_id = "auto_fill_username",
      type = "checkbox",
      title = "option_auto_fill_username_name",
      tooltip = "option_auto_fill_username_tooltip",
      default_value = true,
      sub_widgets = {
        {
          setting_id = "auto_connect",
          type = "checkbox",
          title = "option_auto_connect_name",
          tooltip = "option_auto_connect_tooltip",
          default_value = false,
        },
      },
    },
    {
      setting_id = "debug_enabled",
      type = "checkbox",
      title = "option_debug_enabled_name",
      default_value = false,
    },
  }
}

return mod_data
