--[[
  Copyright 2020 Lucas Schwiderski

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
--]]

local mod = get_mod("twitch_intervention")

local managers = mod:persistent_table("managers")

local template_manager = managers.template_manager
local twitch_manager = managers.twitch_manager

local TwitchVoteUI = TwitchVoteUI

local twitch_ui_definitions = dofile("scripts/ui/hud_ui/twitch_vote_ui_definitions")

local RESULT_TIMER = 3

mod:hook_origin(TwitchVoteUI, "event_finish_vote_ui", function(self, vote_key, winning_index)
  local vote_data = twitch_manager:get_vote_data(vote_key)

  if not vote_data then
    return
  end

  local winning_template_name = vote_data.vote_templates[winning_index]
  self._vote_result = {
    vote_key = vote_key,
    winning_index = winning_index,
    winning_template_name = winning_template_name,
    vote_template = template_manager:get(winning_template_name)
  }

  local vote_type = vote_data.vote_type
  if vote_type == "standard_vote" then
    self:show_ui("standard_vote_result")
  elseif vote_type == "multiple_choice" then
    self:show_ui("multiple_choice_result")
  end
end)

mod:hook_origin(TwitchVoteUI, "start_standard_vote", function(self, vote_template_a_name, vote_template_b_name, vote_inputs, vote_key)
  local vote_template_a = template_manager:get(vote_template_a_name)

  fassert(vote_template_a, "[TwitchVoteUI] Could not find any vote template for %s", vote_template_a_name)

  local vote_template_b = template_manager:get(vote_template_b_name)

  fassert(vote_template_b, "[TwitchVoteUI] Could not find any vote template for %s", vote_template_b_name)

  mod:debug("[TwitchVoteUI.start_standard_vote] Vote %s", vote_template_a and vote_template_a.text)
  mod:debug("[TwitchVoteUI.start_standard_vote] Vote %s", vote_template_b and vote_template_b.text)

  local vote_data = twitch_manager:get_vote_data(vote_key)
  local vote = {
    vote_type = "standard_vote",
    vote_template_a = table.clone(vote_template_a),
    vote_template_b = table.clone(vote_template_b),
    inputs = vote_inputs or {
      "#a",
      "#b"
    },
    vote_key = vote_key,
    timer = vote_data.timer
  }
  self.active = true
  self._active_vote = vote

  self:show_ui("standard_vote")
end)

mod:hook_origin(TwitchVoteUI, "start_multiple_choice_vote", function(self, vote_template_name, vote_inputs, vote_key)
  local vote_template = template_manager:get(vote_template_name)

  if not vote_template then
    mod:error("[TwitchVoteUI.start_multiple_choice_vote] Could not find any vote template for %s", vote_template_name)
    return
  end

  mod:debug("[TwitchVoteUI.start_multiple_choice_vote] Vote '%s'", vote_template and vote_template.text)

  local vote_data = twitch_manager:get_vote_data(vote_key)
  local vote = {
    vote_type = "multiple_choice",
    vote_template = table.clone(vote_template),
    inputs = vote_inputs or {
      "#a",
      "#b",
      "#c",
      "#d",
      "#e"
    },
    vote_key = vote_key,
    timer = vote_data.timer
  }
  self.active = true
  self._active_vote = vote

  self:show_ui("multiple_choice_vote")
end)

mod:hook_origin(TwitchVoteUI, "_show_standard_vote_result", function(self)
  self._fade_out = false
  local vote_result = self._vote_result

  assert(vote_result)

  self._widgets = {}
  local widgets = twitch_ui_definitions.widgets.standard_vote_result

  for widget_name, widget_data in pairs(widgets) do
    self._widgets[widget_name] = UIWidget.init(widget_data)
  end

  self._widgets.result_text.style.text.localize = false
  self._widgets.result_text.style.text_shadow.localize = false
  self._widgets.result_description_text.style.text.localize = false
  self._widgets.result_description_text.style.text_shadow.localize = false

  self._result_timer = RESULT_TIMER
  local winning_template_name = vote_result.winning_template_name

  assert(winning_template_name)

  local winning_template = template_manager:get(winning_template_name)

  local cost = winning_template.cost
  self:_play_winning_sfx(cost)

  -- Set icon of the results widget
  self._widgets.result_icon.content.texture_id = winning_template.texture_id
  -- Set size of the results widget icon
  self._ui_scenegraph.sv_result_icon.size = winning_template.texture_size
  -- Set whether to show the frame around the template's icon
  self._widgets.result_icon_rect.content.visible = winning_template.use_frame_texture or false
  -- Set the result widget's text
  self._widgets.result_text.content.text = winning_template.text
end)

-- Disable automatic localization of vote template names in TwitchVoteUI
mod:hook_safe(TwitchVoteUI, "_show_standard_vote", function(self)
  self._widgets.vote_text_a.style.text.localize = false
  self._widgets.vote_text_a.style.text_shadow.localize = false
  self._widgets.vote_text_b.style.text.localize = false
  self._widgets.vote_text_b.style.text_shadow.localize = false
end)
mod:hook_safe(TwitchVoteUI, "_show_multiple_choice_vote", function(self)
  self._widgets.vote_text.style.text.localize = false
  self._widgets.vote_text.style.text_shadow.localize = false
end)
mod:hook_safe(TwitchVoteUI, "_show_multiple_choice_result", function(self)
  self._widgets.result_text.style.text.localize = false
  self._widgets.result_text.style.text_shadow.localize = false
  self._widgets.winner_text.style.text.localize = false
  self._widgets.winner_text.style.text_shadow.localize = false
end)
