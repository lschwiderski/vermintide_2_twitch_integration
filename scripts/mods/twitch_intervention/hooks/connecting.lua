--[[
  Copyright 2018-2019 Lucas Schwiderski

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
--]]

local mod = get_mod("twitch_intervention")

mod:hook_safe(StartGameWindowTwitchLogin, "create_ui_elements", function(self)
  local widget = self._widgets_by_name["frame_widget"]
  local style = widget.style["twitch_name"]
  -- Reduce a large gap on the left
  style.offset = { 5, 10, 10 }
  -- Correctly position the caret after the letter
  style.caret_offset = { -6, -4, 0 }

  local twitch_username = mod:get("twitch_username")
  if mod:get("auto_fill_username") and twitch_username and string.len(twitch_username) > 0 then
    local content = widget.content
    content.twitch_name = twitch_username
    content.caret_index = string.len(twitch_username) + 1
  end
end)

mod:hook(StartGameWindowTwitchLogin, "on_exit", function(func, self, ...)
  mod:set("twitch_username", self._widgets_by_name["frame_widget"].content.twitch_name)
  return func(self, ...)
end)
