--[[
  Copyright 2020 Lucas Schwiderski

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
--]]

local mod = get_mod("twitch_intervention")

-- Keep root level references for efficiency
local managers = mod:persistent_table("managers")

local twitch_manager = managers.twitch_manager
local template_manager = managers.template_manager

local command_name = "twitch"
local command_description = mod:localize("command_twitch_description")

local function show_help()
  mod:echo(
    "/twitch <action>\n\n" ..
    "Available actions:\n" ..
    "help Show this help message\n" ..
    "vote <option> Guarantee a vote option"
  )
end

local function action_vote(option_name)
  local current_vote = twitch_manager._current_vote
  if not current_vote then
    return
  end

  local vote_data = twitch_manager:get_vote_data(current_vote)
  if not vote_data then
    return
  end

  for index, option_string in ipairs(vote_data.option_strings) do
    if string.find(option_string, option_name) then
      vote_data.options[index] = 999999
      mod:echo("Voted for option %s.", option_name)
    end
  end
end

local function action_force(name)
  template_manager:force(name)
end

local function action_reset_managers()
  twitch_manager:reset()
  template_manager:reset()
  managers.buff_manager:reset()
end

local function action_pause()
  twitch_manager:toggle_paused()
end

local function command_function(action, ...)
  if action == "vote" then
    action_vote(...)
  elseif action == "force" then
    action_force(...)
  elseif action == "reset_managers" then
    action_reset_managers(...)
  elseif action == "pause" then
    action_pause(...)
  else
    show_help()
  end
end

mod:command(command_name, command_description, command_function)
