--[[
  Copyright 2020 Lucas Schwiderski

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
--]]

local mod = get_mod("twitch_intervention")

local function create_command_function(option)
  option = string.lower(option)
  return function()
    local twitch_manager = Managers.twitch
    local current_vote = twitch_manager._current_vote or twitch_manager._vote_queue[1]

    if not current_vote then
      mod:echo("There is currently no active vote to vote for")
      return
    end

    local vote_data = twitch_manager:get_vote_data(current_vote)

    if not vote_data then
      mod:error("Couldn't get vote data for vote %s", current_vote)
      return
    end

    for index, option_string in ipairs(vote_data.option_strings) do
      if string.find(option_string, option) then
        twitch_manager:vote_for_option(
          current_vote,
          Managers.player:local_player():name(),
          index
        )
        mod:echo("You voted successfully for option %s", option)
      end
    end
  end
end

local vote_options = {
  "a", "b", "c", "d", "e",
  "A", "B", "C", "D", "E",
}

for _, vote_option in ipairs(vote_options) do
  mod:command(vote_option, mod:localize("command_vote_" .. string.lower(vote_option) .. "_description"), create_command_function(vote_option))
end
