--[[
  Copyright 2020 Lucas Schwiderski

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
--]]

local mod = get_mod("twitch_intervention")

-- Keep root level references for efficiency
local managers = mod:persistent_table("managers")

local buff_manager = managers.buff_manager
local hook_extender = managers.hook_extender

local function debug_print(message, ...)
  if DEBUG_TWITCH or mod:get("debug_enabled") then
    print("[Twitch] " .. string.format(message, ...))
  end
end

local pickup_items_lookup = {
  wpn_side_objective_tome_01 = "tome",
  wpn_grimoire_01 = "grimoire",
}

local function add_item(is_server, player_unit, pickup_type, destroy)
  local player_manager = Managers.player
  local player = player_manager:owner(player_unit)

  if player then
    local local_bot_or_human = not player.remote

    if local_bot_or_human then
      local network_manager = Managers.state.network
      local network_transmit = network_manager.network_transmit
      local inventory_extension = ScriptUnit.extension(player_unit, "inventory_system")
      local pickup_settings = AllPickups[pickup_type]
      local slot_name = pickup_settings.slot_name
      local item_name = pickup_settings.item_name
      local slot_data = inventory_extension:get_slot_data(slot_name)

      if slot_data then
        local item_data = slot_data.item_data
        local item_template = BackendUtils.get_item_template(item_data)
        local pickup_item_to_spawn = (item_template.pickup_data and item_template.pickup_data.pickup_name)
          or pickup_items_lookup[item_template.name]

        if pickup_item_to_spawn and (destroy ~= true) then
          local pickup_spawn_type = "dropped"
          local pickup_name_id = NetworkLookup.pickup_names[pickup_item_to_spawn]
          local pickup_spawn_type_id = NetworkLookup.pickup_spawn_types[pickup_spawn_type]
          local position = POSITION_LOOKUP[player_unit]

          network_transmit:send_rpc_server(
            "rpc_spawn_pickup_with_physics",
            pickup_name_id,
            position,
            Quaternion.identity(),
            pickup_spawn_type_id
          )
        end
      end

      local item_data = ItemMasterList[item_name]
      local unit_template = nil
      local extra_extension_init_data = {}

      inventory_extension:destroy_slot(slot_name)
      inventory_extension:add_equipment(slot_name, item_data, unit_template, extra_extension_init_data)

      local go_id = Managers.state.unit_storage:go_id(player_unit)
      local slot_id = NetworkLookup.equipment_slots[slot_name]
      local item_id = NetworkLookup.item_names[item_name]
      local weapon_skin_id = NetworkLookup.weapon_skins["n/a"]

      if is_server then
        network_transmit:send_rpc_clients("rpc_add_equipment", go_id, slot_id, item_id, weapon_skin_id)
      else
        network_transmit:send_rpc_server("rpc_add_equipment", go_id, slot_id, item_id, weapon_skin_id)
      end

      local wielded_slot_name = inventory_extension:get_wielded_slot_name()

      if wielded_slot_name == slot_name then
        CharacterStateHelper.stop_weapon_actions(inventory_extension, "picked_up_object")
        inventory_extension:wield(slot_name)
      end
    end
  end
end

local function get_randomized_template_option(initial_number, lower, upper)
  if not mod:get(mod.SETTING_NAMES.RANDOMIZE_TEMPLATE_OUTCOME) then
    return initial_number
  end

  lower = lower or initial_number
  upper = upper or lower

  if upper then
    upper = upper * (mod:get(mod.SETTING_NAMES.RANDOMIZATION_MULTIPLIER) or 1)
  end

  if lower >= upper then
    return lower
  end

  return math.random(lower, upper)
end

local function get_selected_player(vote_index)
  local players = Managers.player:human_and_bot_players()
  local selected_display_name = SPProfiles[vote_index].display_name

  for _, player in pairs(players) do
    local profile_index = player:profile_index()
    local profile = SPProfiles[profile_index]
    local display_name = profile.display_name

    if display_name == selected_display_name then
      return player
    end
  end
end

local function apply_to_selected_player(vote_index, func)
  local player = get_selected_player(vote_index)
  if not player then
    return nil
  end

  local unit = player.player_unit

  if Unit.alive(unit) then
    mod:pcall(func, player, unit)
  end
end

local function apply_to_all_players(func)
  local players = Managers.player:human_and_bot_players()

  for _, player in pairs(players) do
    repeat
      local unit = player.player_unit

      if mod:get("exclude_bot_players") and player.bot_player then
        break
      end

      if mod:get("exclude_dead_players") then
        if not unit then
          break
        end

        local status_extension = ScriptUnit.extension(unit, "status_system")
        if status_extension.ready_for_assisted_respawn or status_extension.assisted_respawning then
          break
        end
      end

      if Unit.alive(unit) then
        mod:pcall(func, player, unit)
      end
    until true
  end
end

local function get_applicable_players(exclude_dead)
  local players = {}

  local exclude_bot = mod:get("exclude_bot_players")
  exclude_dead = exclude_dead or mod:get("exclude_dead_players")

  for _, player in pairs(Managers.player:human_and_bot_players()) do
    repeat
      if exclude_bot and player.bot_player then
        break
      end

      if exclude_dead then
        local player_unit = player.player_unit
        -- When a player dies and hasn't respawned yet, they don't have a unit in the world
        if not player_unit or not Unit.alive(player_unit) then
          break
        end

        local status_extension = ScriptUnit.extension(player.player_unit, "status_system")
        if status_extension.ready_for_assisted_respawn or status_extension.assisted_respawning then
          break
        end
      end

      table.insert(players, player)
    until true
  end

  return players
end

local function build_horde_spawn_list(difficulty)
  local spawn_list = {}
  local breed_type = ({ "skave", "chaos" })[math.random(1, 2)]

  mod:debug("[TwitchInterventionTemplates] Building horde spawn list with breed '%s' on '%s'", breed_type, difficulty)

  local trash_amount_per_difficulty = {
    normal = {
      16,
      22
    },
    hard = {
      22,
      28
    },
    harder = {
      28,
      36
    },
    hardest = {
      36,
      42
    },
  }

  local range_num_trash_enemies = trash_amount_per_difficulty[difficulty] or trash_amount_per_difficulty.hardest
  local num_trash_enemies = get_randomized_template_option(range_num_trash_enemies[1], range_num_trash_enemies[1], range_num_trash_enemies[2])

  local trash_breed = ({
    skaven = "skaven_slave",
    chaos = "chaos_fanatic",
  })[breed_type]

  for _ = 1, num_trash_enemies do
    table.insert(spawn_list, trash_breed)
  end

  if difficulty == "cataclysm" then
    local elite_amount = get_randomized_template_option(4, 3, 9)
    local meat_amount = get_randomized_template_option(6, 4, 12)

    local elite_breeds = ({
      skaven = { "skaven_stormvermin", "skave_stormvermin_with_shield", "skave_plague_monk" },
      chaos = { "chaos_berzerker", "chaos_raider", "chaos_warrior"},
    })[breed_type]
    local num_elite_breeds = #elite_breeds

    local meat_breeds = ({
      skaven = { "skaven_clan_rat", "skaven_clan_rat_with_shield" },
      chaos = { "chaos_marauder", "chaos_marauder_with_shield"},
    })[breed_type]
    local num_meat_breeds = #meat_breeds

    for _ = 1, elite_amount do
      table.insert(spawn_list, elite_breeds[math.random(1, num_elite_breeds)])
    end

    for _ = 1, meat_amount do
      table.insert(spawn_list, meat_breeds[math.random(1, num_meat_breeds)])
    end
  end

  return spawn_list
end

local hooks_buff = {
  duration = 30,
  name = "hooks_buff",
  start = function(state)
    for _, hook_id in ipairs(state.args) do
      hook_extender:enable_hook(hook_id)
    end
  end,
  finish = function(state)
    for _, hook_id in ipairs(state.args) do
      hook_extender:disable_hook(hook_id)
    end
  end,
  cancel = "finish",
}

local function get_hooks_buff()
  if not buff_manager:has_buff(hooks_buff.name) then
    buff_manager:register_buff(hooks_buff)
  end

  return hooks_buff
end

local function get_player_career_name(player)
  if player.career_name then
    return player:career_name()
  end

  return SPProfiles[player:profile_index()].careers[player:career_index()].name
end

return {
  get_randomized_template_option = get_randomized_template_option,
  apply_to_selected_player = apply_to_selected_player,
  get_player_career_name = get_player_career_name,
  get_applicable_players = get_applicable_players,
  build_horde_spawn_list = build_horde_spawn_list,
  apply_to_all_players = apply_to_all_players,
  get_selected_player = get_selected_player,
  get_hooks_buff = get_hooks_buff,
  debug_print = debug_print,
  add_item = add_item,
}
