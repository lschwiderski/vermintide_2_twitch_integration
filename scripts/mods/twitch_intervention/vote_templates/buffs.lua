--[[
  Copyright 2020 Lucas Schwiderski

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
--]]

local mod = get_mod("twitch_intervention")

-- Keep root level references for efficiency
local managers = mod:persistent_table("managers")

local buff_manager = managers.buff_manager
local hook_extender = managers.hook_extender

local helpers = dofile("scripts/mods/twitch_intervention/vote_templates/helpers")

local apply_to_selected_player = helpers.apply_to_selected_player
local get_player_career_name = helpers.get_player_career_name
local get_applicable_players = helpers.get_applicable_players
local apply_to_all_players = helpers.apply_to_all_players
local get_hooks_buff = helpers.get_hooks_buff
local debug_print = helpers.debug_print

local position_lookup = POSITION_LOOKUP

local buff_vote_templates = {}

-- Vanilla Templates

buff_vote_templates.add_speed_potion_buff = {
  cost = -200,
  use_frame_texture = true,
  texture_id = "twitch_icon_boon_of_speed",
  text = Localize("twitch_vote_speed_potion_buff_all"),
  texture_size = {
    70,
    70
  },
  on_success = function (is_server)
    if is_server then
      debug_print("[TWITCH VOTE] Speed boosting all players")

      local buff_extension = Managers.state.entity:system("buff_system")
      local server_controlled = false

      apply_to_all_players(function(_, unit)
        buff_extension:add_buff(unit, "twitch_speed_boost", unit, server_controlled)
      end)
    end
  end
}

buff_vote_templates.add_damage_potion_buff = {
  cost = -200,
  use_frame_texture = true,
  texture_id = "twitch_icon_boon_of_strength",
  text = Localize("twitch_vote_damage_potion_buff_all"),
  texture_size = {
    70,
    70
  },
  condition_func = function()
    return mod:get("twitch_allow_positive_votes")
  end,
  on_success = function (is_server)
    if is_server then
      debug_print("[TWITCH VOTE] Damage boosting all players")

      local buff_extension = Managers.state.entity:system("buff_system")
      local server_controlled = false

      apply_to_all_players(function(_, unit)
        buff_extension:add_buff(unit, "twitch_damage_boost", unit, server_controlled)
      end)
    end
  end
}

buff_vote_templates.add_cooldown_potion_buff = {
  cost = -200,
  use_frame_texture = true,
  texture_id = "twitch_icon_boon_of_concentration",
  text = Localize("twitch_vote_cooldown_potion_buff_all"),
  texture_size = {
    70,
    70
  },
  condition_func = function()
    return mod:get("twitch_allow_positive_votes")
  end,
  on_success = function (is_server)
    if is_server then
      debug_print("[TWITCH VOTE] Cooldown boosting all players")

      local buff_extension = Managers.state.entity:system("buff_system")
      local server_controlled = false

      apply_to_all_players(function(_, unit)
        buff_extension:add_buff(unit, "twitch_cooldown_reduction_boost", unit, server_controlled)
      end)
    end
  end
}

buff_vote_templates.grimoire_health_debuff = {
  cost = 200,
  use_frame_texture = true,
  texture_id = "twitch_icon_curse_of_the_rat",
  text = Localize("twitch_vote_grimoire_health_debuff_all"),
  texture_size = {
    70,
    70
  },
  condition_func = function()
    return mod:get("twitch_allow_positive_votes")
  end,
  on_success = function (is_server)
    if is_server then
      debug_print("[TWITCH VOTE] Adding grimoire health debuff")

      local buff_extension = Managers.state.entity:system("buff_system")
      local server_controlled = false

      apply_to_all_players(function(_, unit)
        buff_extension:add_buff(unit, "twitch_grimoire_health_debuff", unit, server_controlled)
      end)
    end
  end
}

-- Currently broken. Not sure why, since this is identical to FS's vanilla Twitch implementation
buff_vote_templates.no_overcharge_no_ammo_reloads = {
  cost = -200,
  use_frame_texture = true,
  texture_id = "twitch_icon_guns_blazing",
  text = Localize("twitch_vote_twitch_no_overcharge_no_ammo_reloads_all"),
  texture_size = {
    70,
    70
  },
  condition_func = function()
    return false and mod:get("twitch_allow_positive_votes")
  end,
  on_success = function (is_server)
    if is_server then
      debug_print("[TWITCH VOTE] Adding no overcharge/no ammo reloads buff")

      local buff_system = Managers.state.entity:system("buff_system")
      local server_controlled = false

      apply_to_all_players(function(_, unit)
        buff_system:add_buff(unit, "twitch_no_overcharge_no_ammo_reloads", unit, server_controlled)

        -- In case the player used up all his ammo, the buff wouldn't do anything, so we give them one extra
        local weapon_slot = "slot_ranged"
        local ammo_amount = 1
        local inventory_extension = ScriptUnit.extension(unit, "inventory_system")
        local slot_data = inventory_extension:get_slot_data(weapon_slot)
        local right_unit_1p = slot_data.right_unit_1p
        local left_unit_1p = slot_data.left_unit_1p
        local right_hand_ammo_extension = ScriptUnit.has_extension(right_unit_1p, "ammo_system")
        local left_hand_ammo_extension = ScriptUnit.has_extension(left_unit_1p, "ammo_system")
        local ammo_extension = right_hand_ammo_extension or left_hand_ammo_extension

        if ammo_extension then
          ammo_extension:add_ammo(ammo_amount)
        end
      end)
    end
  end
}

buff_vote_templates.health_regen = {
  cost = -200,
  use_frame_texture = true,
  texture_id = "twitch_icon_blessing_of_regeneration",
  text = Localize("twitch_vote_health_regen_all"),
  texture_size = {
    70,
    70
  },
  condition_func = function()
    return mod:get("twitch_allow_positive_votes")
  end,
  on_success = function (is_server)
    if is_server then
      debug_print("[TWITCH VOTE] Adding health regen for all")

      local buff_extension = Managers.state.entity:system("buff_system")
      local server_controlled = false

      apply_to_all_players(function(_, unit)
        buff_extension:add_buff(unit, "twitch_health_regen", unit, server_controlled)
      end)
    end
  end
}

buff_vote_templates.health_degen = {
  cost = 100,
  use_frame_texture = true,
  texture_id = "twitch_icon_blood_loss",
  multiple_choice = true,
  text = Localize("twitch_vote_health_degen_all"),
  texture_size = {
    70,
    70
  },
  on_success = function (is_server, vote_index)
    if is_server then
      debug_print("[TWITCH VOTE] Adding health degen for one")

      local buff_extension = Managers.state.entity:system("buff_system")
      local server_controlled = false

      apply_to_selected_player(vote_index, function(_, unit)
        buff_extension:add_buff(unit, "twitch_health_degen", unit, server_controlled)
      end)
    end
  end
}

buff_vote_templates.twitch_vote_activate_root = {
  cost = 100,
  use_frame_texture = true,
  texture_id = "twitch_icon_root_player",
  multiple_choice = true,
  text = Localize("display_name_twitch_root"),
  texture_size = {
    70,
    70
  },
	condition_func = function ()
		local total_intensity = Managers.state.conflict.pacing:get_pacing_intensity()

		return total_intensity >= 80
	end,
  on_success = function (is_server, vote_index)
    if is_server then
      debug_print("[TWITCH VOTE] Adding root for one")

      local buff_extension = Managers.state.entity:system("buff_system")
      local server_controlled = false

      apply_to_selected_player(vote_index, function(_, unit)
        buff_extension:add_buff(unit, "twitch_vote_buff_root", unit, server_controlled)
      end)
    end
  end
}

buff_vote_templates.twitch_vote_activate_root_all = {
  cost = 200,
  use_frame_texture = true,
  texture_id = "twitch_icon_root_all_players",
  text = Localize("display_name_twitch_root_all"),
  texture_size = {
    70,
    70
  },
	condition_func = function ()
		local total_intensity = Managers.state.conflict.pacing:get_pacing_intensity()

		return total_intensity >= 80
	end,
  on_success = function (is_server, vote_index)
    if is_server then
      debug_print("[TWITCH VOTE] Adding root for all")

      local buff_extension = Managers.state.entity:system("buff_system")
      local server_controlled = false

      apply_to_all_players(vote_index, function(_, unit)
        buff_extension:add_buff(unit, "twitch_vote_buff_root", unit, server_controlled)
      end)
    end
  end
}

buff_vote_templates.twitch_vote_activate_hemmohage = {
  cost = 200,
  use_frame_texture = true,
  texture_id = "twitch_icon_hemmohage",
  multiple_choice = true,
  text = Localize("display_name_hemmoraghe"),
  texture_size = {
    70,
    70
  },
  on_success = function (is_server, vote_index)
    if is_server then
      debug_print("[TWITCH VOTE] Adding hemmoraghe for one")

      local buff_extension = Managers.state.entity:system("buff_system")
      local server_controlled = false

      apply_to_selected_player(vote_index, function(_, unit)
        buff_extension:add_buff(unit, "twitch_vote_buff_hemmoraghe", unit, server_controlled)
      end)
    end
  end
}

buff_vote_templates.twitch_vote_full_temp_hp = {
  cost = -200,
  use_frame_texture = true,
  texture_id = "twitch_icon_shield",
  text = Localize("display_name_twitch_full_temp_hp"),
  texture_size = {
    70,
    70
  },
  condition_func = function()
    return mod:get("twitch_allow_positive_votes")
  end,
  on_success = function (is_server, vote_index)
    if is_server then
      debug_print("[TWITCH VOTE] Adding twitch_vote_full_temp_hp")

      apply_to_selected_player(vote_index, function(_, unit)
        local health_extension = ScriptUnit.extension(unit, "health_system")
        local heal_amount = health_extension:get_max_health()

        DamageUtils.heal_network(unit, unit, heal_amount, "healing_draught_temp_health")
      end)
    end
  end
}

buff_vote_templates.twitch_vote_infinite_bombs = {
  cost = -200,
  use_frame_texture = true,
  texture_id = "twitch_icon_infinite_bomb",
  multiple_choice = true,
  text = Localize("display_name_twitch_infinite_bombs"),
  texture_size = {
    70,
    70
  },
  condition_func = function()
    return mod:get("twitch_allow_positive_votes")
  end,
  on_success = function (is_server, vote_index)
    if is_server then
      debug_print("[TWITCH VOTE] Adding twitch_vote_infinite_bombs for one")

      local buff_extension = Managers.state.entity:system("buff_system")
      local server_controlled = false

      apply_to_selected_player(vote_index, function(_, unit)
        buff_extension:add_buff(unit, "twitch_vote_buff_infinite_bombs", unit, server_controlled)
      end)
    end
  end
}

buff_vote_templates.twitch_vote_invincibility = {
  cost = -200,
  use_frame_texture = true,
  texture_id = "twitch_icon_invincibility",
  multiple_choice = true,
  text = Localize("display_name_twitch_invincibility"),
  texture_size = {
    70,
    70
  },
	condition_func = function ()
		local total_intensity = Managers.state.conflict.pacing:get_pacing_intensity()

		return mod:get("twitch_allow_positive_votes") and total_intensity >= 100
	end,
  on_success = function (is_server, vote_index)
    if is_server then
      debug_print("[TWITCH VOTE] Adding twitch_vote_invincibility for one")

      local buff_extension = Managers.state.entity:system("buff_system")
      local server_controlled = false

      apply_to_selected_player(vote_index, function(_, unit)
        buff_extension:add_buff(unit, "twitch_vote_buff_invincibility", unit, server_controlled)
      end)
    end
  end
}

-- Custom Templates

local hooks_buff = get_hooks_buff()

local one_punch_hook_id = hook_extender:register_hook("GenericHealthExtension", "add_damage")
local one_punch_ogre_hook_id = hook_extender:register_hook("RatOgreHealthExtension", "add_damage")
local one_punch_troll_hook_id = hook_extender:register_hook("ChaosTrollHealthExtension", "add_damage")
buff_vote_templates.one_punch_man = {
  cost = 100,
  use_frame_texture = true,
  texture_id = "kerillian_maidenguard_slower_heavy_hits",
  multiple_choice = true,
  text = mod:localize("twitch_vote_one_punch_man"),
  texture_size = {
    70,
    70,
  },
  condition_func = function()
    return mod:get("twitch_allow_positive_votes")
  end,
  on_success = function (is_server, vote_index)
    if not is_server then
      return
    end

    debug_print("[TWITCH VOTE] Superpowers for sale right now")

    apply_to_selected_player(vote_index, function(player, player_unit)
      local hook_fn = function(func, self, attacker_unit, damage_amount, ...)
        if player_unit == attacker_unit then
          -- Set troll's state so we kill in one hit
          self.state = "wounded"
          damage_amount = self:current_health()
        end

        return func(self, attacker_unit, damage_amount, ...)
      end

      hook_extender:init_hook(one_punch_hook_id, hook_fn)
      hook_extender:init_hook(one_punch_ogre_hook_id, hook_fn)
      hook_extender:init_hook(one_punch_troll_hook_id, hook_fn)

      local options = {
        duration = 45,
        timer_id = get_player_career_name(player) .. ".one_punch_man",
      }
      buff_manager:start_buff(hooks_buff.name, options, one_punch_hook_id, one_punch_ogre_hook_id, one_punch_troll_hook_id)
    end)
  end
}

local no_sound_buff = {
  name = "no_sound",
  duration = 25,
  start = function()
    Managers.music:set_master_volume(0)
  end,
  finish = function()
    Managers.music:set_master_volume(Application.user_setting("master_bus_volume") or 90)
  end,
  cancel = "finish",
  single_instance = true,
}

buff_manager:register_buff(no_sound_buff)

buff_vote_templates.no_sound = {
  cost = 100,
  use_frame_texture = true,
  texture_id = "victor_witchhunter_regrowth",
  text = mod:localize("twitch_vote_no_sound"),
  texture_size = {
    70,
    70
  },
  on_success = function (is_server)
    if is_server then
      debug_print("[TWITCH VOTE] Muting all game sound")
    end

    buff_manager:start_buff(no_sound_buff.name)
  end
}

-- TODO: Will crash on mission end and the user's settings will be gone
--[[
local randomize_input_affected_settings = {
  -- Lookup part, mainly for selecting one with `math.random`
  "mouse_look_invert_y",
  "mouse_look_sensitivity",
  "gamepad_look_invert_y",

  -- Actual data
  mouse_look_invert_y = {
    initial_value = Application.user_setting("mouse_look_invert_y") or false,
    possible_values = { true, false }
  },
  mouse_look_sensitivity = {
    initial_value = Application.user_setting("mouse_look_sensitivity") or 0,
    range = { -10, 10 },
  },
  gamepad_look_invert_y = {
    initial_value = Application.user_setting("gamepad_look_invert_y") or false,
    possible_values = { true, false }
  },
}

local randomize_input_buff = {
  name = "randomize_input",
  duration = 60,
  tick_rate = 5,
  start = function(state)
    state.count = 1
    state.settings = {}
    for setting in pairs(randomize_input_affected_settings) do
      state.settings[setting] = Application.user_setting(setting)
    end
  end,
  update = function(state)
    if state.count == 1 then
      local chosen_setting_name = randomize_input_affected_settings[math.random(#randomize_input_affected_settings)]
      local setting = randomize_input_affected_settings[chosen_setting_name]

      local new_value
      if setting.range then
        new_value = (math.random() * (setting.range[2] - setting.range[1])) + setting.range[1]
      elseif setting.possible_values then
        new_value = setting.possible_values[math.random(#setting.possible_values)]
      end

      if new_value ~= nil then
        Application.set_user_setting(chosen_setting_name, new_value)
        Application.save_user_settings()
      end
    elseif state.count > 2 then
      state.count = 0
    end

    state.count = state.count + 1
  end,
  finish = function(state)
    for setting, value in pairs(state.settings) do
      Application.set_user_setting(setting, value)
      Application.save_user_settings()
    end
  end,
  cancel = "finish",
}

buff_manager:register_buff(randomize_input_buff)

buff_vote_templates.randomize_input = {
  cost = 70,
  use_frame_texture = true,
  texture_id = "bardin_slayer_activated_ability_impact_damage",
  text = mod:localize("twitch_vote_randomize_input"),
  multiple_choice = true,
  texture_size = {
    70,
    70
  },
  on_success = function(is_server, vote_index)
    if is_server then
      debug_print("[TWITCH VOTE] Changing a player's input settings at random")
    end

    apply_to_selected_player(vote_index, function(player)
      local local_player = Managers.player:local_player()

      if local_player == player then
        buff_manager:start_buff(randomize_input_buff.name, get_player_career_name(player))
      end
    end)
  end
}
--]]

local swap_player_positions_buff = {
  name = "swap_player_positions",
  duration = 50,
  single_instance = true,
  update = function(state, _, time)
    local next_tick = state.next_tick

    if not next_tick then
      state.next_tick = time + math.random(3, 10)
      return
    elseif time < next_tick and time < 50 then
      return
    end

    state.next_tick = nil

    local always_exclude_dead = true
    local players = get_applicable_players(always_exclude_dead)
    local num_players = #players

    local positions = {}
    local lookup = {}

    -- If there's only one, try to find a random position near him
    if num_players == 1 then
      local nav_world = Managers.state.entity:system("ai_system"):nav_world()
      local player = players[1]
      local center_pos = Unit.local_position(player.player_unit, 0)

      local pos = ConflictUtils.get_spawn_pos_on_circle(nav_world, center_pos, 10, 40, 30)

      if pos then
        positions[1] = pos
        lookup[player] = 1
      end
    elseif num_players == 2 then
      -- If there's two players, swap them
      local player1 = players[1]
      local player2 = players[2]
      positions[1] = Unit.local_position(player1.player_unit, 0)
      positions[2] = Unit.local_position(player2.player_unit, 0)
      lookup[player1] = 2
      lookup[player2] = 1
    else
      -- For more than 2, shuffle their positions between all of them
      for _, player in pairs(players) do
        local position = Unit.local_position(player.player_unit, 0)
        table.insert(positions, position)
        lookup[player] = #positions
      end

      table.shuffle(positions)
    end

    local network_manager = Managers.state.network
    for player, position_id in pairs(lookup) do
      local unit = player.player_unit
      local unit_id = network_manager:unit_game_object_id(unit)
      local target_position = positions[position_id]

      if target_position then
        if not player.remote then
          mod:debug("[swap_player_positions_buff.update] Teleporting local unit")
          ScriptUnit.extension(unit, "locomotion_system"):teleport_to(target_position)
        else
          mod:debug("[swap_player_positions_buff.update] Sending RPC 'rpc_teleport_unit_to' to remote player")
          network_manager.network_transmit:send_rpc(
            "rpc_teleport_unit_to",
            player.peer_id,
            unit_id,
            target_position,
            Unit.local_rotation(unit, 0)
          )
        end
      end
    end
  end,
}

buff_manager:register_buff(swap_player_positions_buff)

buff_vote_templates.swap_player_positions = {
  cost = 150,
  use_frame_texture = true,
  texture_id = "kerillian_shade_activated_ability_no_break_on_ranged",
  text = mod:localize("twitch_vote_swap_player_positions"),
  texture_size = {
    70,
    70
  },
  on_success = function(is_server)
    if not is_server then
      return
    end

    debug_print("[TWITCH VOTE] Shuffling player positions")
    buff_manager:start_buff(swap_player_positions_buff.name)
  end
}

local increased_friendly_fire_hook_id = hook_extender:register_hook("PlayerUnitHealthExtension", "add_damage")
buff_vote_templates.increased_friendly_fire = {
  cost = 150,
  use_frame_texture = true,
  texture_id = "kerillian_waywatcher_headshot_multiplier",
  text = mod:localize("twitch_vote_increased_friendly_fire"),
  multiple_choice = true,
  texture_size = {
    70,
    70
  },
  on_success = function(is_server, vote_index)
    if not is_server then
      return
    end

    debug_print("[TWITCH VOTE] Increasing friendly fire")

    apply_to_selected_player(vote_index, function(player, player_unit)
      hook_extender:init_hook(increased_friendly_fire_hook_id, function(func, self, attacker_unit, damage_amount, ...)
        if attacker_unit == player_unit and attacker_unit ~= self.unit then
          damage_amount = self:current_health()
        end

        return func(self, attacker_unit, damage_amount, ...)
      end)

      local options = {
        timer_id = get_player_career_name(player),
        duration = 30,
      }
      buff_manager:start_buff(hooks_buff.name, options, increased_friendly_fire_hook_id)
    end)
  end
}

local no_ult_cooldown_buff = {
  name = "no_ult_cooldown",
  duration = 20,
  update = function(state)
    local player_unit = state.args[1]

    if Unit.alive(player_unit) then
      local career_extension = ScriptUnit.extension(player_unit, "career_system")

      local current_cooldown = career_extension:current_ability_cooldown()
      if current_cooldown > 0 then
        career_extension:reduce_activated_ability_cooldown(current_cooldown)
      end
    end
  end,
}

buff_manager:register_buff(no_ult_cooldown_buff)

buff_vote_templates.no_ult_cooldown = {
  cost = -75,
  use_frame_texture = true,
  texture_id = "markus_mercenary_activated_ability_cooldown",
  text = mod:localize("twitch_vote_no_ult_cooldown"),
  multiple_choice = true,
  texture_size = {
    70,
    70
  },
  credits = {
    source = "twitch",
    name = "Subutaikhan",
  },
  condition_func = function()
    return mod:get("twitch_allow_positive_votes")
  end,
  on_success = function(_, vote_index)
    debug_print("[TWITCH VOTE] Removed player ability cooldown")

    apply_to_selected_player(vote_index, function(player, player_unit)
      local options = {
        timer_id = get_player_career_name(player),
      }
      buff_manager:start_buff(no_ult_cooldown_buff.name, options, player_unit)
    end)
  end
}

local maximum_crit_chance_hook_id = hook_extender:register_hook("ActionUtils", "get_critical_strike_chance")
buff_vote_templates.maximum_crit_chance = {
  cost = -50,
  use_frame_texture = true,
  texture_id = "markus_mercenary_crit_chance",
  text = mod:localize("twitch_vote_maximum_crit_chance"),
  multiple_choice = true,
  texture_size = {
    70,
    70
  },
  condition_func = function()
    return mod:get("twitch_allow_positive_votes")
  end,
  on_success = function(_, vote_index)
    debug_print("[TWITCH VOTE] Increasing player's crit chance")

    apply_to_selected_player(vote_index, function(player, player_unit)
      hook_extender:init_hook(maximum_crit_chance_hook_id, function(func, unit, ...)
        if unit == player_unit then
          return 100
        else
          return func(unit, ...)
        end
      end)

      local options = {
        duration = 45,
        timer_id = get_player_career_name(player),
      }
      buff_manager:start_buff(hooks_buff.name, options, maximum_crit_chance_hook_id)
    end)
  end
}

local increased_attack_speed_hook_id = hook_extender:register_hook("ActionUtils", "get_action_time_scale")
buff_vote_templates.increased_attack_speed = {
  cost = -50,
  use_frame_texture = true,
  texture_id = "victor_witchhunter_attack_speed_on_ping_target_killed",
  text = mod:localize("twitch_vote_increased_attack_speed"),
  multiple_choice = true,
  texture_size = {
    70,
    70
  },
  condition_func = function()
    return mod:get("twitch_allow_positive_votes")
  end,
  on_success = function(_, vote_index)
    debug_print("[TWITCH VOTE] Increasing player's attack speed")

    apply_to_selected_player(vote_index, function(player, player_unit)
      hook_extender:init_hook(increased_attack_speed_hook_id, function(func, unit, ...)
        if unit == player_unit then
          return 2.5
        else
          return func(unit, ...)
        end
      end)

      local options = {
        timer_id = get_player_career_name(player),
        duration = 30,
      }
      buff_manager:start_buff(hooks_buff.name, options, increased_attack_speed_hook_id)
    end)
  end
}

local apply_movement_buffs_buff = {
  name = "apply_movement_buffs",
  duration = 30,
  start = function(state)
    local unit = state.args[1]
    local buffs = state.args[2]
    local params = state.args[3]

    for idx, buff in ipairs(buffs) do
      BuffFunctionTemplates.functions.apply_movement_buff(unit, buff, params[idx])
    end
  end,
  finish = function(state)
    local unit = state.args[1]
    local buffs = state.args[2]
    local params = state.args[3]

    for idx, buff in ipairs(buffs) do
      BuffFunctionTemplates.functions.remove_movement_buff(unit, buff, params[idx])
    end
  end,
  cancel = "finish",
}

buff_manager:register_buff(apply_movement_buffs_buff)

buff_vote_templates.increased_move_speed = {
  cost = -50,
  use_frame_texture = true,
  texture_id = "bardin_ranger_movement_speed",
  text = mod:localize("twitch_vote_increased_move_speed"),
  multiple_choice = true,
  texture_size = {
    70,
    70,
  },
  condition_func = function()
    return mod:get("twitch_allow_positive_votes")
  end,
  on_success = function(_, vote_index)
    debug_print("[TWITCH VOTE] Increasing player's move speed")

    local fake_buff = {
      template = {
        path_to_movement_setting_to_modify = {
          "move_speed",
        },
      },
    }

    local params = {
      multiplier = 1.8,
    }

    apply_to_selected_player(vote_index, function(player, player_unit)
      local options = {
        duration = 40,
        timer_id = get_player_career_name(player) .. ".move_speed",
      }

      buff_manager:start_buff(
        apply_movement_buffs_buff.name,
        options,
        player_unit,
        { fake_buff },
        { params }
      )
    end)
  end
}

buff_vote_templates.increased_dodge_range = {
  cost = -50,
  use_frame_texture = true,
  texture_id = "kerillian_shade_dodge_range",
  text = mod:localize("twitch_vote_increased_dodge_range"),
  multiple_choice = true,
  texture_size = {
    70,
    70,
  },
  condition_func = function()
    return mod:get("twitch_allow_positive_votes")
  end,
  on_success = function(_, vote_index)
    debug_print("[TWITCH VOTE] Increasing player's dodging")

    local distance_buff = {
      template = {
        path_to_movement_setting_to_modify = {
          "dodging",
          "distance_modifier",
        },
      },
    }

    local speed_buff = {
      template = {
        path_to_movement_setting_to_modify = {
          "dodging",
          "speed_modifier",
        },
      },
    }

    local params = {
      {
        multiplier = 3,
      },
      {
        multiplier = 3,
      }
    }

    local buffs = {
      distance_buff,
      speed_buff,
    }

    apply_to_selected_player(vote_index, function(player, player_unit)
      local options = {
        timer_id = get_player_career_name(player) .. ".dodge",
      }
      buff_manager:start_buff(
        apply_movement_buffs_buff.name,
        options,
        player_unit,
        buffs,
        params
      )
    end)
  end
}

buff_vote_templates.increased_jump = {
  cost = -25,
  use_frame_texture = true,
  texture_id = "bardin_slayer_activated_ability",
  text = mod:localize("twitch_vote_increased_jump"),
  multiple_choice = true,
  texture_size = {
    70,
    70,
  },
  condition_func = function()
    return mod:get("twitch_allow_positive_votes")
  end,
  on_success = function(_, vote_index)
    debug_print("[TWITCH VOTE] Increasing player's jumping")

    local buffs = {
      {
        template = {
          path_to_movement_setting_to_modify = {
            "jump",
            "initial_vertical_speed",
          },
        },
      },
      {
        template = {
          path_to_movement_setting_to_modify = {
            "fall",
            "heights",
            "MIN_FALL_DAMAGE_HEIGHT"
          },
        },
      },
      {
        template = {
          path_to_movement_setting_to_modify = {
            "fall",
            "heights",
            "HARD_LANDING_FALL_HEIGHT"
          },
        },
      }
    }

    local MULTIPLIER = 4
    local params = {
      { multiplier = MULTIPLIER },
      { multiplier = MULTIPLIER },
      { multiplier = MULTIPLIER },
    }

    apply_to_selected_player(vote_index, function(player, player_unit)
      local options = {
        timer_id = get_player_career_name(player) .. ".jump",
      }
      buff_manager:start_buff(
        apply_movement_buffs_buff.name,
        options,
        player_unit,
        buffs,
        params
      )
    end)
  end
}

return buff_vote_templates
