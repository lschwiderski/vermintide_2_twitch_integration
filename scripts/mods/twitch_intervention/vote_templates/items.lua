--[[
  Copyright 2020 Lucas Schwiderski

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
--]]

local mod = get_mod("twitch_intervention")

-- Keep root level references for efficiency
local managers = mod:persistent_table("managers")

local buff_manager = managers.buff_manager
local hook_extender = managers.hook_extender

local helpers = require("scripts/mods/twitch_intervention/vote_templates/helpers")

local debug_print = helpers.debug_print
local add_item = helpers.add_item
local apply_to_selected_player = helpers.apply_to_selected_player

local item_vote_templates = {}

item_vote_templates.first_aid_kit = {
  cost = -100,
  use_frame_texture = true,
  texture_id = "twitch_icon_medical_supplies",
  multiple_choice = true,
  text = Localize("twitch_give_first_aid_kit_one"),
  texture_size = {
    70,
    70
  },
  condition_func = function()
    return mod:get("twitch_allow_positive_votes")
  end,
  on_success = function (is_server, vote_index)
    local selected_display_name = SPProfiles[vote_index].display_name
    local players = Managers.player:human_and_bot_players()

    for _, player in pairs(players) do
      local profile_index = player:profile_index()
      local profile = SPProfiles[profile_index]
      local display_name = profile.display_name

      if display_name == selected_display_name then
        debug_print(string.format("[TWITCH VOTE] giving first aid kit to  %s", Localize(profile.character_name)))

        local unit = player.player_unit

        if Unit.alive(unit) then
          add_item(is_server, unit, "first_aid_kit")
        end

        break
      end
    end
  end
}
item_vote_templates.healing_draught = {
  cost = -100,
  use_frame_texture = true,
  texture_id = "twitch_icon_healing_draught",
  multiple_choice = true,
  text = Localize("twitch_give_healing_draught_one"),
  texture_size = {
    70,
    70
  },
  condition_func = function()
    return mod:get("twitch_allow_positive_votes")
  end,
  on_success = function (is_server, vote_index)
    local selected_display_name = SPProfiles[vote_index].display_name
    local players = Managers.player:human_and_bot_players()

    for _, player in pairs(players) do
      local profile_index = player:profile_index()
      local profile = SPProfiles[profile_index]
      local display_name = profile.display_name

      if display_name == selected_display_name then
        debug_print(string.format("[TWITCH VOTE] giving health potion to  %s", Localize(profile.character_name)))

        local unit = player.player_unit

        if Unit.alive(unit) then
          add_item(is_server, unit, "healing_draught")
        end

        break
      end
    end
  end
}
item_vote_templates.damage_boost_potion = {
  cost = -50,
  use_frame_texture = true,
  texture_id = "twitch_icon_potion_of_strength",
  multiple_choice = true,
  text = Localize("twitch_give_damage_boost_potion_one"),
  texture_size = {
    70,
    70
  },
  condition_func = function()
    return mod:get("twitch_allow_positive_votes")
  end,
  on_success = function (is_server, vote_index)
    local selected_display_name = SPProfiles[vote_index].display_name
    local players = Managers.player:human_and_bot_players()

    for _, player in pairs(players) do
      local profile_index = player:profile_index()
      local profile = SPProfiles[profile_index]
      local display_name = profile.display_name

      if display_name == selected_display_name then
        debug_print(string.format("[TWITCH VOTE] giving damage boost potion to  %s", Localize(profile.character_name)))

        local unit = player.player_unit

        if Unit.alive(unit) then
          add_item(is_server, unit, "damage_boost_potion")
        end

        break
      end
    end
  end
}
item_vote_templates.speed_boost_potion = {
  cost = -50,
  use_frame_texture = true,
  texture_id = "twitch_icon_potion_of_speed",
  multiple_choice = true,
  text = Localize("twitch_give_speed_boost_potion_one"),
  texture_size = {
    70,
    70
  },
  condition_func = function()
    return mod:get("twitch_allow_positive_votes")
  end,
  on_success = function (is_server, vote_index)
    local selected_display_name = SPProfiles[vote_index].display_name
    local players = Managers.player:human_and_bot_players()

    for _, player in pairs(players) do
      local profile_index = player:profile_index()
      local profile = SPProfiles[profile_index]
      local display_name = profile.display_name

      if display_name == selected_display_name then
        debug_print(string.format("[TWITCH VOTE] giving speed boost potion to  %s", Localize(profile.character_name)))

        local unit = player.player_unit

        if Unit.alive(unit) then
          add_item(is_server, unit, "speed_boost_potion")
        end

        break
      end
    end
  end
}
item_vote_templates.cooldown_reduction_potion = {
  cost = -50,
  use_frame_texture = true,
  texture_id = "twitch_icon_potion_of_concentration",
  multiple_choice = true,
  text = Localize("twitch_give_cooldown_reduction_potion_one"),
  texture_size = {
    70,
    70
  },
  condition_func = function()
    return mod:get("twitch_allow_positive_votes")
  end,
  on_success = function (is_server, vote_index)
    local selected_display_name = SPProfiles[vote_index].display_name
    local players = Managers.player:human_and_bot_players()

    for _, player in pairs(players) do
      local profile_index = player:profile_index()
      local profile = SPProfiles[profile_index]
      local display_name = profile.display_name

      if display_name == selected_display_name then
        debug_print(string.format("[TWITCH VOTE] giving cooldown reduction potion to  %s", Localize(profile.character_name)))

        local unit = player.player_unit

        if Unit.alive(unit) then
          add_item(is_server, unit, "cooldown_reduction_potion")
        end

        break
      end
    end
  end
}
item_vote_templates.frag_grenade_t1 = {
  cost = -100,
  use_frame_texture = true,
  texture_id = "twitch_icon_bomb",
  multiple_choice = true,
  text = Localize("twitch_give_frag_grenade_t1_one"),
  texture_size = {
    70,
    70
  },
  condition_func = function()
    return mod:get("twitch_allow_positive_votes")
  end,
  on_success = function (is_server, vote_index)
    local selected_display_name = SPProfiles[vote_index].display_name
    local players = Managers.player:human_and_bot_players()

    for _, player in pairs(players) do
      local profile_index = player:profile_index()
      local profile = SPProfiles[profile_index]
      local display_name = profile.display_name

      if display_name == selected_display_name then
        debug_print(string.format("[TWITCH VOTE] giving frag grenade t1 to  %s", Localize(profile.character_name)))

        local unit = player.player_unit

        if Unit.alive(unit) then
          add_item(is_server, unit, "frag_grenade_t1")
        end

        break
      end
    end
  end
}
item_vote_templates.fire_grenade_t1 = {
  cost = -100,
  use_frame_texture = true,
  texture_id = "twitch_icon_incediary_bomb",
  multiple_choice = true,
  text = Localize("twitch_give_fire_grenade_t1_one"),
  texture_size = {
    70,
    70
  },
  condition_func = function()
    return mod:get("twitch_allow_positive_votes")
  end,
  on_success = function (is_server, vote_index)
    local selected_display_name = SPProfiles[vote_index].display_name
    local players = Managers.player:human_and_bot_players()

    for _, player in pairs(players) do
      local profile_index = player:profile_index()
      local profile = SPProfiles[profile_index]
      local display_name = profile.display_name

      if display_name == selected_display_name then
        debug_print(string.format("[TWITCH VOTE] giving fire grenade t1 to  %s", Localize(profile.character_name)))

        local unit = player.player_unit

        if Unit.alive(unit) then
          add_item(is_server, unit, "fire_grenade_t1")
        end

        break
      end
    end
  end
}

-- Custom Templates

local change_wielded_weapon_buff = {
  name = "change_wielded_weapon",
  duration = 120,
  start = function(state)
    local player = state.args[1]
    local player_unit = state.args[2]

    local inventory_ext = ScriptUnit.extension(player_unit, "inventory_system")

    local wielded_slot_name = inventory_ext:get_wielded_slot_name()
    local slot_type = (wielded_slot_name == "slot_ranged" and "ranged") or "melee"

    local backend_items = Managers.backend:get_interface("items")
    local all_items = backend_items:get_filtered_items("can_wield_by_current_career and slot_type == " .. slot_type)
    local backend_id = all_items[math.random(#all_items)].backend_id
    local career_name = player:career_name()
    local slot_name = "slot_" .. slot_type

    local previous_item = BackendUtils.get_loadout_item(career_name, slot_name)

    if previous_item then
      state.previous_backend_id = previous_item.backend_id
    end

    local career_extension = inventory_ext.career_extension

    CharacterStateHelper.stop_weapon_actions(inventory_ext, "weapon_wielded")
    CharacterStateHelper.stop_career_abilities(career_extension, "weapon_wielded")

    backend_items:set_loadout_item(backend_id, career_name, slot_name)
    inventory_ext:create_equipment_in_slot(slot_name, backend_id)

    state.slot_name = slot_name
    state.career_name = career_name
  end,
  finish = function(state)
    local player_unit = state.args[2]
    local backend_id = state.previous_backend_id
    local career_name = state.career_name
    local slot_name = state.slot_name

    local inventory_ext = ScriptUnit.extension(player_unit, "inventory_system")

    local career_extension = inventory_ext.career_extension

    CharacterStateHelper.stop_weapon_actions(inventory_ext, "weapon_wielded")
    CharacterStateHelper.stop_career_abilities(career_extension, "weapon_wielded")

    Managers.backend:get_interface("items"):set_loadout_item(backend_id, career_name, slot_name)
    inventory_ext:create_equipment_in_slot(slot_name, backend_id)
  end,
  cancel = "finish",
}

buff_manager:register_buff(change_wielded_weapon_buff)

item_vote_templates.change_wielded_weapon = {
  cost = 100,
  use_frame_texture = true,
  texture_id = "victor_bountyhunter_reload_speed",
  multiple_choice = true,
  text = mod:localize("twitch_vote_change_wielded_weapon"),
  texture_size = {
    70,
    70,
  },
  on_success = function (is_server, vote_index)
    if is_server then
      debug_print("[TWITCH VOTE] Changing a player's wielded weapon to random owned one")
    end

    -- In order for the picked player to pick from their own inventory,
    -- this is run on both servers and clients and it is checked for
    -- matching the selected player

    apply_to_selected_player(vote_index, function(player, player_unit)
      local local_player = Managers.player:local_player()

      -- Only execute on the chosen player's machine
      if player.bot_player or local_player.player_unit ~= player_unit then
        return
      end

      local options = {
        timer_id = player:career_name() .. ".change_wielded_weapon",
      }
      buff_manager:start_buff(change_wielded_weapon_buff.name, options, player, player_unit)
    end)
  end
}

return item_vote_templates
