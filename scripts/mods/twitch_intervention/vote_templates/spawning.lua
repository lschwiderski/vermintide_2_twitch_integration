--[[
  Copyright 2020 Lucas Schwiderski

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
--]]

local mod = get_mod("twitch_intervention")

-- Keep root level references for efficiency
local managers = mod:persistent_table("managers")

local buff_manager = managers.buff_manager
local hook_extender = managers.hook_extender

local helpers = require("scripts/mods/twitch_intervention/vote_templates/helpers")

local position_lookup = POSITION_LOOKUP

local debug_print = helpers.debug_print
local randomize = helpers.get_randomized_template_option
local apply_to_all_players = helpers.apply_to_all_players
local get_hooks_buff = helpers.get_hooks_buff
local build_horde_spawn_list = helpers.build_horde_spawn_list

local function spawn_barrels_on_unit(barrel_type, amount, lit, unit)
  local pickup_system = Managers.state.entity:system("pickup_system")

  amount = amount or 2
  lit = lit ~= false
  local pickup_name = barrel_type
  local pickup_settings = AllPickups[pickup_name]
  local spawn_type = "dropped"

  local damage_amount = 1
  local damage_type = "forced"
  local hit_zone = "full"

  local damage_direction = Vector3.forward()

  if lit then
    debug_print("[TWITCH VOTE] Spawning lit %s", barrel_type)
  else
    debug_print("[TWITCH VOTE] Spawning %s", barrel_type)
  end

  local unit_position = Unit.local_position(unit, 0)

  for i = 1, amount do
    -- "Hello, little bomb."
    local pickup_unit = pickup_system:_spawn_pickup(
      pickup_settings,
      pickup_name,
      unit_position,
      Quaternion.identity(),
      true,
      spawn_type
    )

    if lit then
      local hit_position = Unit.world_position(pickup_unit, 0)

      -- Go suicide, little barrel
      ScriptUnit.extension(pickup_unit, "health_system"):add_damage(
        pickup_unit,
        damage_amount,
        hit_zone,
        damage_type,
        hit_position,
        damage_direction
      )
    end
  end
end

local function spawn_barrels_infront(barrel_type, amount, lit, distance)
  local pickup_system = Managers.state.entity:system("pickup_system")

  amount = amount or 2
  lit = lit ~= false
  distance = distance or 2.5
  local pickup_name = barrel_type
  local pickup_settings = AllPickups[pickup_name]
  local spawn_type = "dropped"

  local damage_amount = 1
  local damage_type = "forced"
  local hit_zone = "full"

  local damage_direction = Vector3.forward()
  local up_vector = Vector3.up()

  if lit then
    debug_print("[TWITCH VOTE] Spawning lit %s", barrel_type)
  else
    debug_print("[TWITCH VOTE] Spawning %s", barrel_type)
  end

  local players = Managers.player:players()

  for _, player in pairs(players) do
    repeat
      if not player:is_player_controlled() then
        break
      end

      local player_unit = player.player_unit
      local player_position = Unit.local_position(player_unit, 0)
      local player_rotation = Unit.local_rotation(player_unit, 0)

      local forward = Vector3.normalize(Quaternion.forward(player_rotation))

      for i = 1, amount do
        -- Create angles in 30° steps with the player's looking direction as center
        local angle = (i - ((amount + 1) / 2.0)) * 0.5235988
        local rotator = Quaternion(up_vector, angle)

        local spawn_direction = Quaternion.rotate(rotator, forward)

        -- "Hello, little bomb."
        local pickup_unit = pickup_system:_spawn_pickup(
          pickup_settings,
          pickup_name,
          player_position + (spawn_direction * distance),
          Quaternion.identity(),
          true,
          spawn_type
        )

        if lit then
          -- Go suicide, little barrel
          ScriptUnit.extension(pickup_unit, "health_system"):add_damage(pickup_unit, damage_amount, hit_zone, damage_type, damage_direction)
        end
      end
    until true
  end
end

local function spawn_barrels_distributed(barrel_type, amount, lit)
  local pickup_system = Managers.state.entity:system("pickup_system")

  amount = amount or 20
  local pickup_name = barrel_type
  local pickup_settings = AllPickups[pickup_name]
  local spawn_type = "dropped"

  local damage_amount = 1
  local damage_type = "grenade"
  local hit_zone_name = "full"
  local damage_direction = Vector3.forward()

  local nav_world = Managers.state.entity:system("ai_system"):nav_world()

  local players = Managers.player:human_and_bot_players()

  for _, player in pairs(players) do
    repeat
      local player_unit = player.player_unit

      if not (Unit.alive(player_unit) and ScriptUnit.extension(player_unit, "health_system"):is_alive()) then
        break
      end

      local center_pos = position_lookup[player_unit]

      for _ = 1, amount / 4 do
        local spawn_pos = ConflictUtils.get_spawn_pos_on_circle(nav_world, center_pos, 2, 30, 30)

        if spawn_pos then
          -- "Hello, little bomb."
          local pickup_unit = pickup_system:_spawn_pickup(
            pickup_settings,
            pickup_name,
            spawn_pos,
            Quaternion.identity(),
            true,
            spawn_type
          )

          if lit then
            local hit_position = Unit.world_position(pickup_unit, 0)

            -- Go suicide, little barrel
            ScriptUnit.extension(pickup_unit, "health_system"):add_damage(
              pickup_unit,
              damage_amount,
              hit_zone_name,
              damage_type,
              hit_position,
              damage_direction
            )
          end
        end
      end
    until true
  end
end

local function spawn_custom_horde(spawn_list)
	local side = Managers.state.side:get_side_from_name("dark_pact")
	local side_id = side.side_id
	local only_ahead = false
	local conflict_director = Managers.state.conflict

  conflict_director.horde_spawner:execute_custom_horde(spawn_list, only_ahead, side_id)
end

local function spawn_hidden(breed, amount_of_enemies)
	local conflict_director = Managers.state.conflict

	for i = 1, amount_of_enemies, 1 do
		local hidden_pos = conflict_director.specials_pacing:get_special_spawn_pos()

		conflict_director:spawn_one(breed, hidden_pos)
	end
end

local function get_random_boss_amount(default)
  default = default or 1
  local is_active = mod:get(mod.SETTING_NAMES.RANDOMIZE_TEMPLATE_OUTCOME)
    and mod:get(mod.SETTING_NAMES.RANDOMIZE_BOSS_TEMPLATE_OUTCOME)
  return (is_active and math.random() > 0.95 and (default + 1)) or default
end

local spawning_vote_templates = {}

spawning_vote_templates.spawn_rat_ogre = {
  text = Localize("twitch_vote_spawn_rat_ogre"),
  cost = 180,
  texture_id = "twitch_icon_all_the_rage",
  texture_size = {
    60,
    70
  },
  on_success = function (is_server)
    if is_server then
      debug_print("[TWITCH VOTE] Spawning rat ogre")

      local breed = Breeds.skaven_rat_ogre
      local modifiers = { max_health_modifier = randomize(0.85, 0.8, 1.1) }
      local amount = get_random_boss_amount()

      for i = 1, amount do
        Managers.state.conflict:spawn_one(breed, nil, nil, modifiers)
      end
    end
  end
}
spawning_vote_templates.spawn_stormfiend = {
  text = Localize("twitch_vote_spawn_stormfiend"),
  cost = 180,
  texture_id = "twitch_icon_fire_and_fury",
  texture_size = {
    60,
    70
  },
  on_success = function (is_server)
    if is_server then
      debug_print("[TWITCH VOTE] Spawning stormfiend")

      local breed = Breeds.skaven_stormfiend
      local modifiers = { max_health_modifier = randomize(0.85, 0.8, 1.1) }
      local amount = get_random_boss_amount()

      for i = 1, amount do
        Managers.state.conflict:spawn_one(breed, nil, nil, modifiers)
      end
    end
  end
}
spawning_vote_templates.spawn_chaos_troll = {
  text = Localize("twitch_vote_spawn_chaos_troll"),
  cost = 180,
  texture_id = "twitch_icon_bad_indigestion",
  texture_size = {
    60,
    70
  },
  on_success = function (is_server)
    if is_server then
      debug_print("[TWITCH VOTE] Spawning chaos troll")

      local breed = Breeds.chaos_troll
      local modifiers = { max_health_modifier = randomize(0.85, 0.8, 1.1) }
      local amount = get_random_boss_amount()

      for i = 1, amount do
        Managers.state.conflict:spawn_one(breed, nil, nil, modifiers)
      end
    end
  end
}
spawning_vote_templates.spawn_chaos_spawn = {
  text = Localize("twitch_vote_spawn_chaos_spawn"),
  cost = 180,
  texture_id = "twitch_icon_writhing_horror",
  texture_size = {
    60,
    70
  },
  on_success = function (is_server)
    if is_server then
      debug_print("[TWITCH VOTE] Spawning chaos spawn")

      local breed = Breeds.chaos_spawn
      local modifiers = { max_health_modifier = randomize(0.85, 0.8, 1.1) }
      local amount = get_random_boss_amount()

      for i = 1, amount do
        Managers.state.conflict:spawn_one(breed, nil, nil, modifiers)
      end
    end
  end
}
spawning_vote_templates.twitch_spawn_minotaur = {
  text = Localize("twitch_vote_spawn_minotaur"),
  cost = 180,
  texture_id = "twitch_icon_the_bloodkine_wakes",
  texture_size = {
    60,
    70
  },
  on_success = function (is_server)
    if is_server then
      debug_print("[TWITCH VOTE] Spawning chaos spawn")

      local breed = Breeds.beastmen_minotaur
      local modifiers = { max_health_modifier = randomize(0.85, 0.8, 1.1) }
      local amount = get_random_boss_amount()

      for i = 1, amount do
        Managers.state.conflict:spawn_one(breed, nil, nil, modifiers)
      end
    end
  end
}
spawning_vote_templates.spawn_corruptor_sorcerer = {
  text = Localize("twitch_vote_spawn_corruptor_sorcerer"),
  cost = 150,
  texture_id = "twitch_icon_soul_drinkers",
  texture_size = {
    60,
    70
  },
  on_success = function (is_server)
    if is_server then
      debug_print("[TWITCH VOTE] Spawning group of corruptor sorcerers")

      local amount = randomize(2, 2, 5)
      local breed = Breeds.chaos_corruptor_sorcerer
      spawn_hidden(breed, amount)
    end
  end
}
spawning_vote_templates.spawn_vortex_sorcerer = {
  text = Localize("twitch_vote_spawn_vortex_sorcerer"),
  cost = 100,
  texture_id = "twitch_icon_all_aboard_the_wild_ride",
  texture_size = {
    60,
    70
  },
  on_success = function (is_server)
    if is_server then
      debug_print("[TWITCH VOTE] Spawning group of vortex sorceres")

      local amount = randomize(4, 2)
      local breed = Breeds.chaos_vortex_sorcerer
      spawn_hidden(breed, amount)
    end
  end
}
spawning_vote_templates.spawn_gutter_runner = {
  text = Localize("twitch_vote_spawn_gutter_runner"),
  cost = 150,
  texture_id = "twitch_icon_sneaking_stabbing",
  texture_size = {
    60,
    70
  },
  on_success = function (is_server)
    if is_server then
      debug_print("[TWITCH VOTE] Spawning group of gutter runners")

      local amount = randomize(2, 2, 4)
      local breed = Breeds.skaven_gutter_runner
      spawn_hidden(breed, amount)
    end
  end
}
spawning_vote_templates.spawn_pack_master = {
  text = Localize("twitch_vote_spawn_pack_master"),
  cost = 150,
  texture_id = "twitch_icon_cruel_hooks",
  texture_size = {
    60,
    70
  },
  on_success = function (is_server)
    if is_server then
      debug_print("[TWITCH VOTE] Spawning group of packmasters")

      local amount = randomize(3, 2, 6)
      local breed = Breeds.skaven_pack_master
      spawn_hidden(breed, amount)
    end
  end
}
spawning_vote_templates.spawn_poison_wind_globadier = {
  text = Localize("twitch_vote_spawn_poison_wind_globadier"),
  cost = 100,
  texture_id = "twitch_icon_hold_your_breath",
  texture_size = {
    60,
    70
  },
  on_success = function (is_server)
    if is_server then
      debug_print("[TWITCH VOTE] Spawning group of poison wind globadiers")

      local amount = randomize(3, 3, 7)
      local breed = Breeds.skaven_poison_wind_globadier
      spawn_hidden(breed, amount)
    end
  end
}
spawning_vote_templates.spawn_ratling_gunner = {
  text = Localize("twitch_vote_spawn_ratling_gunner"),
  cost = 100,
  texture_id = "twitch_icon_gunline",
  texture_size = {
    60,
    70
  },
  on_success = function (is_server)
    if is_server then
      debug_print("[TWITCH VOTE] Spawning group of ratling gunners")

      local amount = randomize(4, 3, 6)
      local breed = Breeds.skaven_ratling_gunner
      spawn_hidden(breed, amount)
    end
  end
}
spawning_vote_templates.spawn_warpfire_thrower = {
  text = Localize("twitch_vote_spawn_warpfire_thrower"),
  cost = 100,
  texture_id = "twitch_icon_kill_it_with_fire",
  texture_size = {
    60,
    70
  },
  on_success = function (is_server)
    if is_server then
      debug_print("[TWITCH VOTE] Spawning group of warpfire throwers")

      local amount = randomize(4, 3, 6)
      local breed = Breeds.skaven_warpfire_thrower
      spawn_hidden(breed, amount)
    end
  end
}
spawning_vote_templates.spawn_horde_vector_blob = {
  text = Localize("twitch_vote_spawn_horde"),
  cost = 100,
  texture_id = "twitch_icon_release_the_slaves",
  texture_size = {
    60,
    70
  },
  on_success = function (is_server)
    if is_server then
      debug_print("[TWITCH VOTE] Spawning horde")

      local difficulty = Managers.state.difficulty:get_difficulty()
      local spawn_list = build_horde_spawn_list(difficulty)

      spawn_custom_horde(spawn_list)
    end
  end
}
spawning_vote_templates.spawn_loot_rat_fiesta = {
  text = Localize("twitch_vote_spawn_loot_rat_fiesta"),
  cost = -300,
  texture_id = "twitch_icon_treasure_hunt",
  texture_size = {
    60,
    70
  },
  on_success = function (is_server)
    if is_server then
      debug_print("[TWITCH VOTE] Spawning loot rat fiesta")

      local amount = randomize(10, 8, 13)
      local breed = Breeds.skaven_loot_rat

      for i = 1, amount, 1 do
        Managers.state.conflict:spawn_one(breed)
      end
    end
  end
}
spawning_vote_templates.twitch_spawn_explosive_loot_rats = {
  text = Localize("display_name_explosive_loot_rats"),
  cost = 100,
  texture_id = "twitch_icon_explosive_loot_rats",
  texture_size = {
    60,
    70
  },
  on_success = function (is_server)
    if is_server then
			debug_print("[TWITCH VOTE] Spawning explosive loot rats")

			local amount_of_enemies_per_difficulty = {
				normal = {
					3,
					4
				},
				hard = {
					4,
					6
				},
				harder = {
					6,
					8
				},
				hardest = {
					8,
					10
				}
			}
      local difficulty = Managers.state.difficulty:get_difficulty()
      local range = amount_of_enemies_per_difficulty[difficulty] or amount_of_enemies_per_difficulty.hardest
      local amount = randomize(range[1], range[1], range[2])
      local breed = "skaven_explosive_loot_rat"
      local spawn_list = {}

      for _ = 1, amount, 1 do
        table.insert(spawn_list, breed)
      end

      spawn_custom_horde(spawn_list)
    end
  end
}
spawning_vote_templates.twitch_spawn_plague_monks = {
  text = Localize("twitch_vote_spawn_plague_monks"),
  cost = 100,
  texture_id = "twitch_icon_plague_monk",
  texture_size = {
    60,
    70
  },
  on_success = function (is_server)
    if is_server then
			debug_print("[TWITCH VOTE] Spawning plague_monks")

			local amount_of_enemies_per_difficulty = {
				normal = {
					3,
					4
				},
				hard = {
					4,
					6
				},
				harder = {
					6,
					8
				},
				hardest = {
					8,
					10
				}
			}
      local difficulty = Managers.state.difficulty:get_difficulty()
      local range = amount_of_enemies_per_difficulty[difficulty] or amount_of_enemies_per_difficulty.hardest
      local amount = randomize(range[1], range[1], range[2])
      local breed = "skaven_plague_monk"
      local spawn_list = {}

      for _ = 1, amount, 1 do
        table.insert(spawn_list, breed)
      end

      spawn_custom_horde(spawn_list)
    end
  end
}
spawning_vote_templates.twitch_spawn_berzerkers = {
  text = Localize("twitch_vote_spawn_berzerkers"),
  cost = 100,
  texture_id = "twitch_icon_berzerker",
  texture_size = {
    60,
    70
  },
  on_success = function (is_server)
    if is_server then
			debug_print("[TWITCH VOTE] Spawning chaos_berzerker")

			local amount_of_enemies_per_difficulty = {
				normal = {
					3,
					4
				},
				hard = {
					4,
					6
				},
				harder = {
					6,
					8
				},
				hardest = {
					8,
					10
				}
			}
      local difficulty = Managers.state.difficulty:get_difficulty()
      local range = amount_of_enemies_per_difficulty[difficulty] or amount_of_enemies_per_difficulty.hardest
      local amount = randomize(range[1], range[1], range[2])
      local breed = "chaos_berzerker"
      local spawn_list = {}

      for _ = 1, amount, 1 do
        table.insert(spawn_list, breed)
      end

      spawn_custom_horde(spawn_list)
    end
  end
}
spawning_vote_templates.twitch_spawn_death_squad_storm_vermin = {
  text = Localize("twitch_vote_spawn_death_squad_storm_vermin"),
  cost = 250,
  texture_id = "twitch_icon_blackfurs_on_parade",
	boss_equivalent = true,
  texture_size = {
    60,
    70
  },
  on_success = function (is_server)
    if is_server then
			debug_print("[TWITCH VOTE] Spawning storm vermin patrol")

			local amount_of_enemies_per_difficulty = {
				normal = {
					6,
					8
				},
				hard = {
					8,
					10
				},
				harder = {
					10,
					12
				},
				hardest = {
					12,
					14
				}
			}
      local difficulty = Managers.state.difficulty:get_difficulty()
      local range = amount_of_enemies_per_difficulty[difficulty] or amount_of_enemies_per_difficulty.hardest
      local amount = randomize(range[1], range[1], range[2])
      local breed = "skaven_storm_vermin"
      local spawn_list = {}

      for _ = 1, amount, 1 do
        table.insert(spawn_list, breed)
      end

      spawn_custom_horde(spawn_list)
    end
  end
}
spawning_vote_templates.twitch_spawn_death_squad_chaos_warrior = {
  text = Localize("twitch_vote_spawn_death_squad_chaos_warrior"),
  cost = 250,
  texture_id = "twitch_icon_eavymetal",
	boss_equivalent = true,
  texture_size = {
    60,
    70
  },
  on_success = function (is_server)
    if is_server then
			debug_print("[TWITCH VOTE] Spawning chaos warriors death squad")

			local amount_of_enemies_per_difficulty = {
				normal = {
					4,
					5
				},
				hard = {
					4,
					6
				},
				harder = {
					6,
					8
				},
				hardest = {
					8,
					10
				}
			}
      local difficulty = Managers.state.difficulty:get_difficulty()
      local range = amount_of_enemies_per_difficulty[difficulty] or amount_of_enemies_per_difficulty.hardest
      local amount = randomize(range[1], range[1], range[2])
      local breed = "chaos_warrior"
      local spawn_list = {}

      for _ = 1, amount, 1 do
        table.insert(spawn_list, breed)
      end

      spawn_custom_horde(spawn_list)
    end
  end
}

-- CUSTOM TEMPLATES

local hooks_buff = get_hooks_buff()

-- Currently broken:
-- Multiple barrels in close proximity trying to blow up on the same frame will crash.
-- in `DamageUtils.create_explosion` the system tries to apply effects to all units within explosion radius
-- but some of the barrels already exploded and ended up in a state where the unit was partially deleted but
-- still part of the `PhysicsWorld`.
-- TODO: Either "fix" `DamageUtils.create_explosion` or change the barrel distribution to prevent overlapping
spawning_vote_templates.spawn_lit_explosive_barrels = {
  text = mod:localize("twitch_vote_spawn_lit_explosive_barrels"),
  cost = 10,
  texture_id = "trait_trinket_not_consume_grenade",
  texture_size = {
    60,
    70
  },
  on_success = function (is_server)
    if not (is_server and Managers.state and Managers.state.entity) then
      return
    end

    -- Spawn "not lit" until it's fixed (see above)
    spawn_barrels_distributed("explosive_barrel", 20, false)
  end
}

spawning_vote_templates.spawn_lit_fire_barrels = {
  text = mod:localize("twitch_vote_spawn_lit_fire_barrels"),
  cost = 30,
  texture_id = "trinket_increase_grenade_radius",
  texture_size = {
    60,
    70
  },
  on_success = function (is_server)
    if not (is_server and Managers.state and Managers.state.entity) then
      return
    end

    -- Spawn "not lit" until it's fixed (see above)
    spawn_barrels_distributed("lamp_oil", 12, false)
  end
}

local barrel_on_player_buff = {
  name = "spawn_barrel_on_player",
  duration = 40,
  tick_rate = 4,
  single_instance = true,
  update = function()
    local players = Managers.player:human_players()
    local player_units = {}

    for _, player in pairs(players) do
      repeat
        local player_unit = player.player_unit

        if not player_unit then
          break
        end

        local status_extension = ScriptUnit.extension(player_unit, "status_system")
        if status_extension.ready_for_assisted_respawn or status_extension.assisted_respawning then
          break
        end

        table.insert(player_units, player_unit)
      until true
    end

    local player_unit = player_units[math.random(#player_units)] or Managers.player:local_player().player_unit
    spawn_barrels_on_unit("explosive_barrel", 1, true, player_unit)
  end,
}

buff_manager:register_buff(barrel_on_player_buff)

spawning_vote_templates.spawn_lit_explosive_barrels_for_players = {
  text = mod:localize("twitch_vote_spawn_lit_explosive_barrels_for_players"),
  cost = 40,
  texture_id = "trinket_reduce_radius_by_50_increase_power_by_50",
  texture_size = {
    60,
    70
  },
  on_success = function (is_server)
    if is_server and Managers.state and Managers.state.entity then
      debug_print("[TWITCH VOTE] Spawning lit barrels on random players")
      buff_manager:start_buff(barrel_on_player_buff.name)
    end
  end
}

spawning_vote_templates.spawn_backstabbers = {
  text = mod:localize("twitch_vote_spawn_backstabbers"),
  cost = 80,
  texture_id = "unit_frame_portrait_enemy_clanrat",
  texture_size = {
    60,
    70
  },
  on_success = function (is_server)
    if is_server then
      debug_print("[TWITCH VOTE] Spawning backstabbing enemy for each player")

      local players = Managers.player:human_and_bot_players()

      local distance = 1.5

      -- Make sure there's only one ogre and gutter runner
      local did_ogre_spawn = false
      local did_runner_spawn = false

      for _, player in pairs(players) do
        local breed

        local breed_chance = math.random()
        if breed_chance <= 0.005 and not did_ogre_spawn then
          breed = Breeds.skaven_rat_ogre
          did_ogre_spawn = true
        elseif breed_chance <= 0.055 and not did_runner_spawn then
          breed = Breeds.skaven_gutter_runner
          did_runner_spawn = true
        elseif breed_chance <= 0.205 then
          breed = Breeds.skaven_storm_vermin
        else
          breed = Breeds.skaven_clan_rat
        end

        local player_unit = player.player_unit
        local player_pos = POSITION_LOOKUP[player_unit]
        local player_rot = Unit.local_rotation(player_unit, 0)

        local backwards = Vector3.normalize(Quaternion.forward(player_rot)) * (-1)

        Managers.state.conflict:spawn_queued_unit(
          breed,
          Vector3Box(player_pos + (backwards * distance)),
          QuaternionBox(player_rot),
          "spawn_one"
        )
      end
    end
  end
}

local mini_boss_breeds = {
  Breeds.skaven_rat_ogre,
  Breeds.skaven_stormfiend,
  Breeds.chaos_troll,
  Breeds.chaos_spawn,
  Breeds.beastmen_minotaur,
}

spawning_vote_templates.spawn_all_four_mini_bosses = {
  text = mod:localize("twitch_vote_spawn_all_four_mini_bosses"),
  cost = 80,
  texture_id = "unit_frame_portrait_enemy_chaos_spawn",
  texture_size = {
    60,
    70
  },
  on_success = function (is_server)
    if is_server then
      debug_print("[TWITCH VOTE] Spawning all four (or 5) mini bosses")
      local modifiers = { max_health_modifier = 1/3 }

      for _, breed in ipairs(mini_boss_breeds) do
        Managers.state.conflict:spawn_one(breed, nil, nil, modifiers)
      end
    end
  end
}

local companion_rat_damage_hook_id = hook_extender:register_hook("PlayerUnitHealthExtension", "add_damage")
spawning_vote_templates.companion_rat = {
  text = mod:localize("twitch_vote_companion_rat"),
  cost = 50,
  texture_id = "unit_frame_portrait_enemy_slave_rat",
  texture_size = {
    60,
    70
  },
  credits = {
    source = "twitch",
    name = "besprisornik",
  },
  on_success = function (is_server)
    if not is_server then
      return
    end

    debug_print("[TWITCH VOTE] Spawning companion rat")

    local breed = table.clone(Breeds.skaven_slave)
    breed.max_health = {
      500,
      500,
      750,
      1000,
      1150,
      1400,
      1400,
      1400,
    }
    breed.stagger_immune = true
    breed.size_variation_range = { 0.5, 0.65 }

    breed.run_on_spawn = function(rat_unit)
      hook_extender:init_hook(companion_rat_damage_hook_id, function(func, self, attacker_unit, damage_amount, ...)
        if attacker_unit == rat_unit then
          damage_amount = 1
        end

        return func(self, attacker_unit, damage_amount, ...)
      end)

      hook_extender:enable_hook(companion_rat_damage_hook_id)
    end

    breed.run_on_death = function()
      hook_extender:disable_hook(companion_rat_damage_hook_id)
    end

    -- TODO: Add custom behavior so that the rat will only ever follow one player. Then make it a `multiple_choice` vote
    -- TODO: If possible, make it pingable
    -- TODO: Make it say voice lines constantly

    local state_managers = Managers.state
    local nav_world = state_managers.entity:system("ai_system"):nav_world()
    local side = Managers.state.side:get_side_from_name("heroes")
    local player_positions = side.PLAYER_POSITIONS
    local spawn_pos = ConflictUtils.get_spawn_pos_on_circle(nav_world, player_positions[1], 6, 4, 15)

    -- Couldn't find a close location, so try a far one next
    if not spawn_pos then
      spawn_pos = ConflictUtils.get_spawn_pos_on_circle(nav_world, player_positions[1], 18, 12, 30)
    end

    state_managers.conflict:spawn_one(breed, spawn_pos or nil)
  end
}

local limited_time_spawns_buff = {
  name = "limited_time_spawns",
  duration = 30,
  start = function(state)
    local spawn_combination = state.args[1]
    local units = {}

    local on_spawn_func = function(unit)
      units[#units + 1] = unit

      local health_ext = ScriptUnit.extension(unit, "health_system")
      health_ext.is_invincible = true
    end

    local on_prepare_func = function(_, extension_init_data)
      extension_init_data.health_system.invincible = true
    end

    local conflict_manager = Managers.state.conflict
    for _, comb in ipairs(spawn_combination) do
      for _ = 1, (comb.amount or 1), 1 do
        local optional_data = comb.modifiers or {}
        -- register unit on spawn
        optional_data.spawned_func = on_spawn_func
        optional_data.prepare_func = on_prepare_func

        local breed = comb.breed
        -- Prevent these bosses from dropping a loot dice.
        -- This also prevents a crash in that function when triggered through `conflict_manager:destroy_unit`
        breed.run_on_despawn = nil

        conflict_manager:spawn_one(breed, nil, nil, optional_data)
      end
    end

    state.units = units
  end,
  finish = function(state)
    local conflict_manager = Managers.state.conflict
    local BLACKBOARDS = BLACKBOARDS
    for _, unit in ipairs(state.units) do
      local blackboard = BLACKBOARDS[unit]
      conflict_manager:destroy_unit(unit, blackboard)
    end
  end,
  cancel = "finish",
}

buff_manager:register_buff(limited_time_spawns_buff)

spawning_vote_templates.limited_time_ogres = {
  text = mod:localize("twitch_vote_limited_time_ogres"),
  cost = 100,
  texture_id = "unit_frame_portrait_enemy_rat_ogre",
  texture_size = {
    60,
    70
  },
  condition_func = function()
    return mod:get("twitch_allow_limited_time_votes")
  end,
  on_success = function (is_server)
    if not is_server then
      return
    end

    debug_print("[TWITCH VOTE] Spawning 2 invincible rat ogres on limited time")

    local combination = {
      breed = Breeds.skaven_rat_ogre,
      amount = get_random_boss_amount(2),
    }

    buff_manager:start_buff(limited_time_spawns_buff.name, nil, { combination })
  end
}

spawning_vote_templates.limited_time_chaos_spawns = {
  text = mod:localize("twitch_vote_limited_time_chaos_spawns"),
  cost = 100,
  texture_id = "unit_frame_portrait_enemy_chaos_spawn",
  texture_size = {
    60,
    70
  },
  condition_func = function()
    return mod:get("twitch_allow_limited_time_votes")
  end,
  on_success = function (is_server)
    if not is_server then
      return
    end

    debug_print("[TWITCH VOTE] Spawning 2 invincible chaos spawns on limited time")

    local combination = {
      breed = Breeds.chaos_spawn,
      amount = get_random_boss_amount(2),
    }

    buff_manager:start_buff(limited_time_spawns_buff.name, nil, { combination })
  end
}

spawning_vote_templates.limited_time_trolls = {
  text = mod:localize("twitch_vote_limited_time_trolls"),
  cost = 100,
  texture_id = "unit_frame_portrait_enemy_chaos_troll",
  texture_size = {
    60,
    70
  },
  condition_func = function()
    return mod:get("twitch_allow_limited_time_votes")
  end,
  on_success = function (is_server)
    if not is_server then
      return
    end

    debug_print("[TWITCH VOTE] Spawning 2 invincible chaos trolls on limited time")

    local combination = {
      breed = Breeds.chaos_troll,
      amount = get_random_boss_amount(2),
    }

    buff_manager:start_buff(limited_time_spawns_buff.name, nil, { combination })
  end
}

spawning_vote_templates.limited_time_stormfiends = {
  text = mod:localize("twitch_vote_limited_time_stormfiends"),
  cost = 100,
  texture_id = "unit_frame_portrait_enemy_stormfiend",
  texture_size = {
    60,
    70
  },
  condition_func = function()
    return mod:get("twitch_allow_limited_time_votes")
  end,
  on_success = function (is_server)
    if not is_server then
      return
    end

    debug_print("[TWITCH VOTE] Spawning 2 invincible stormfiends on limited time")

    local combination = {
      breed = Breeds.skaven_stormfiend,
      amount = get_random_boss_amount(2),
    }

    buff_manager:start_buff(limited_time_spawns_buff.name, nil, { combination })
  end
}

spawning_vote_templates.limited_time_skaven_squad = {
  text = mod:localize("twitch_vote_limited_time_skaven_squad"),
  cost = 75,
  texture_id = "unit_frame_portrait_enemy_ratling_gunner",
  texture_size = {
    60,
    70
  },
  condition_func = function()
    return mod:get("twitch_allow_limited_time_votes")
  end,
  on_success = function (is_server)
    if not is_server then
      return
    end

    debug_print("[TWITCH VOTE] Spawning invincible skaven specials on limited time")

    local combination = {
      {
        breed = Breeds.skaven_poison_wind_globadier,
        amount = randomize(2, 2, 4),
      },
      {
        breed = Breeds.skaven_ratling_gunner,
        amount = randomize(2, 2, 4),
      },
      {
        breed = Breeds.skaven_warpfire_thrower,
        amount = randomize(2, 2, 4),
      },
    }

    buff_manager:start_buff(limited_time_spawns_buff.name, nil, combination)
  end
}

spawning_vote_templates.limited_time_chaos_squad = {
  text = mod:localize("twitch_vote_limited_time_chaos_squad"),
  cost = 75,
  texture_id = "unit_frame_portrait_enemy_sorcerer_vortex",
  texture_size = {
    60,
    70
  },
  condition_func = function()
    return mod:get("twitch_allow_limited_time_votes")
  end,
  on_success = function (is_server)
    if not is_server then
      return
    end

    debug_print("[TWITCH VOTE] Spawning invincible chaos specials on limited time")

    local combination = {
      {
        breed = Breeds.chaos_corruptor_sorcerer,
        amount = randomize(2, 2, 3),
      },
      {
        breed = Breeds.chaos_vortex_sorcerer,
        amount = randomize(2, 2, 3),
      },
    }

    buff_manager:start_buff(limited_time_spawns_buff.name, nil, combination)
  end
}

spawning_vote_templates.limited_time_skaven_patrol = {
  text = mod:localize("twitch_vote_limited_time_skaven_patrol"),
  cost = 75,
  texture_id = "unit_frame_portrait_enemy_stormvermin",
  texture_size = {
    60,
    70
  },
  condition_func = function()
    return mod:get("twitch_allow_limited_time_votes")
  end,
  on_success = function (is_server)
    if not is_server then
      return
    end

    debug_print("[TWITCH VOTE] Spawning invincible skaven patrol on limited time")

    local combination = {
      {
        breed = Breeds.skaven_storm_vermin,
        amount = randomize(5, 5, 8),
      },
      {
        breed = Breeds.skaven_storm_vermin_with_shield,
        amount = randomize(2, 2, 4),
      },
      {
        breed = Breeds.skaven_clan_rat,
        amount = randomize(10, 8, 16),
      },
    }

    buff_manager:start_buff(limited_time_spawns_buff.name, nil, combination)
  end
}

spawning_vote_templates.limited_time_chaos_patrol = {
  text = mod:localize("twitch_vote_limited_time_chaos_patrol"),
  cost = 75,
  texture_id = "unit_frame_portrait_enemy_chaos_marauder",
  texture_size = {
    60,
    70
  },
  condition_func = function()
    return mod:get("twitch_allow_limited_time_votes")
  end,
  on_success = function (is_server)
    if not is_server then
      return
    end

    debug_print("[TWITCH VOTE] Spawning invincible chaos patrol on limited time")

    local combination = {
      {
        breed = Breeds.chaos_warrior,
        amount = randomize(3, 3, 8),
      },
      {
        breed = Breeds.chaos_marauder,
        amount = randomize(5, 5, 10),
      },
      {
        breed = Breeds.chaos_raider,
        amount = randomize(3, 3, 10),
      },
    }

    buff_manager:start_buff(limited_time_spawns_buff.name, nil, combination)
  end
}

return spawning_vote_templates
