--[[
  Copyright 2020 Lucas Schwiderski

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
--]]

local mod = get_mod("twitch_intervention")

-- Keep root level references for efficiency
local managers = mod:persistent_table("managers")

local helpers = require("scripts/mods/twitch_intervention/vote_templates/helpers")

local debug_print = helpers.debug_print
local apply_to_all_players = helpers.apply_to_all_players

local function add_buff_to_all_players(buff_name)
	local buff_system = Managers.state.entity:system("buff_system")
	local server_controlled = false

	apply_to_all_players(function(_, unit)
		buff_system:add_buff(unit, buff_name, unit, server_controlled)
	end)
end

local mutator_vote_templates = {}

mutator_vote_templates.twitch_vote_activate_splitting_enemies = {
	text = Localize("display_name_mutator_splitting_enemies"),
	cost = 200,
	texture_id = "twitch_icon_splitting_enemies",
	description = Localize("description_mutator_splitting_enemies"),
	texture_size = {
		60,
		70
	},
	condition_func = function (current_vote)
		return not Managers.state.game_mode._mutator_handler:has_activated_mutator("splitting_enemies") and mod:get("twitch_allow_mutators")
	end,
	on_success = function (is_server)
		if is_server then
			local mutator_handler = Managers.state.game_mode._mutator_handler
			local mutator_name = "splitting_enemies"
			local duration = 30 * mod:get("twitch_mutators_duration_multiplier")

			debug_print(string.format("[TWITCH VOTE] Activating mutator %s", mutator_name))
			mutator_handler:initialize_mutators({
				mutator_name
			})
			mutator_handler:activate_mutator(mutator_name, duration, "activated_by_twitch")
			add_buff_to_all_players("twitch_mutator_buff_splitting_enemies")
		end
	end
}
mutator_vote_templates.twitch_vote_activate_leash = {
	text = Localize("display_name_mutator_leash"),
	cost = 200,
	texture_id = "twitch_icon_leash",
	description = Localize("description_mutator_leash"),
	texture_size = {
		60,
		70
	},
	condition_func = function (current_vote)
		local num_human_players = Managers.player:num_human_players()

		return num_human_players > 1 and mod:get("twitch_allow_mutators") and not Managers.state.game_mode._mutator_handler:has_activated_mutator("leash")
	end,
	on_success = function (is_server)
		if is_server then
			local mutator_handler = Managers.state.game_mode._mutator_handler
			local mutator_name = "leash"
			local duration = 30 * mod:get("twitch_mutators_duration_multiplier")

			debug_print(string.format("[TWITCH VOTE] Activating mutator %s", mutator_name))
			mutator_handler:initialize_mutators({
				mutator_name
			})
			mutator_handler:activate_mutator(mutator_name, duration, "activated_by_twitch")
			add_buff_to_all_players("twitch_mutator_buff_leash")
		end
	end
}
mutator_vote_templates.twitch_vote_activate_slayer_curse = {
	text = Localize("display_name_mutator_slayer_curse"),
	cost = 200,
	texture_id = "twitch_icon_slayer_curse",
	description = Localize("description_mutator_slayer_curse"),
	texture_size = {
		60,
		70
	},
	condition_func = function (current_vote)
		return not Managers.state.game_mode._mutator_handler:has_activated_mutator("slayer_curse") and mod:get("twitch_allow_mutators")
	end,
	on_success = function (is_server)
		if is_server then
			local mutator_handler = Managers.state.game_mode._mutator_handler
			local mutator_name = "slayer_curse"
			local duration = 30 * mod:get("twitch_mutators_duration_multiplier")

			debug_print(string.format("[TWITCH VOTE] Activating mutator %s", mutator_name))
			mutator_handler:initialize_mutators({
				mutator_name
			})
			mutator_handler:activate_mutator(mutator_name, duration, "activated_by_twitch")
			add_buff_to_all_players("twitch_mutator_buff_slayers_curse")
		end
	end
}
mutator_vote_templates.twitch_vote_activate_bloodlust = {
	text = Localize("display_name_mutator_bloodlust"),
	cost = 200,
	texture_id = "twitch_icon_bloodlust",
	description = Localize("description_mutator_bloodlust"),
	texture_size = {
		60,
		70
	},
	condition_func = function (current_vote)
		return not Managers.state.game_mode._mutator_handler:has_activated_mutator("bloodlust") and mod:get("twitch_allow_mutators")
	end,
	on_success = function (is_server)
		if is_server then
			local mutator_handler = Managers.state.game_mode._mutator_handler
			local mutator_name = "bloodlust"
			local duration = 30 * mod:get("twitch_mutators_duration_multiplier")

			debug_print(string.format("[TWITCH VOTE] Activating mutator %s", mutator_name))
			mutator_handler:initialize_mutators({
				mutator_name
			})
			mutator_handler:activate_mutator(mutator_name, duration, "activated_by_twitch")
			add_buff_to_all_players("twitch_mutator_buff_bloodlust")
		end
	end
}
mutator_vote_templates.twitch_vote_activate_realism = {
	text = Localize("display_name_mutator_realism"),
	cost = 200,
	texture_id = "twitch_icon_realism",
	description = Localize("description_mutator_realism"),
	texture_size = {
		60,
		70
	},
	condition_func = function (current_vote)
		return not Managers.state.game_mode._mutator_handler:has_activated_mutator("realism") and mod:get("twitch_allow_mutators")
	end,
	on_success = function (is_server)
		if is_server then
			local mutator_handler = Managers.state.game_mode._mutator_handler
			local mutator_name = "realism"
			local duration = 60 * mod:get("twitch_mutators_duration_multiplier")

			debug_print(string.format("[TWITCH VOTE] Activating mutator %s", mutator_name))
			mutator_handler:initialize_mutators({
				mutator_name
			})
			mutator_handler:activate_mutator(mutator_name, duration, "activated_by_twitch")
		end
	end
}
mutator_vote_templates.twitch_vote_activate_darkness = {
	text = Localize("display_name_mutator_darkness"),
	cost = 200,
	texture_id = "twitch_icon_darkness",
	description = Localize("description_mutator_darkness"),
	texture_size = {
		60,
		70
	},
	condition_func = function (current_vote)
		return not Managers.state.game_mode._mutator_handler:has_activated_mutator("darkness") and not Managers.state.game_mode._mutator_handler:has_activated_mutator("twitch_darkness") and not Managers.state.game_mode._mutator_handler:has_activated_mutator("night_mode") and not Managers.state.game_mode.level_transition_handler:get_current_environment_variation_name() and mod:get("twitch_allow_mutators")
	end,
	on_success = function (is_server)
		if is_server then
			local mutator_handler = Managers.state.game_mode._mutator_handler
			local mutator_name = "twitch_darkness"
			local duration = 30 * mod:get("twitch_mutators_duration_multiplier")

			debug_print(string.format("[TWITCH VOTE] Activating mutator %s", mutator_name))
			mutator_handler:initialize_mutators({
				mutator_name
			})
			mutator_handler:activate_mutator(mutator_name, duration, "activated_by_twitch")
		end
	end
}
mutator_vote_templates.twitch_vote_activate_ticking_bomb = {
	text = Localize("display_name_mutator_ticking_bomb"),
	cost = 100,
	texture_id = "twitch_icon_ticking_bomb",
	description = Localize("description_mutator_ticking_bomb"),
	texture_size = {
		60,
		70
	},
	condition_func = function (current_vote)
		return not Managers.state.game_mode._mutator_handler:has_activated_mutator("ticking_bomb") and mod:get("twitch_allow_mutators")
	end,
	on_success = function (is_server)
		if is_server then
			local mutator_handler = Managers.state.game_mode._mutator_handler
			local mutator_name = "ticking_bomb"
			local duration = 30 * mod:get("twitch_mutators_duration_multiplier")

			debug_print(string.format("[TWITCH VOTE] Activating mutator %s", mutator_name))
			mutator_handler:initialize_mutators({
				mutator_name
			})
			mutator_handler:activate_mutator(mutator_name, duration, "activated_by_twitch")
			add_buff_to_all_players("twitch_mutator_buff_ticking_bomb")
		end
	end
}
mutator_vote_templates.twitch_vote_activate_lightning_strike = {
	text = Localize("display_name_lightning_strike"),
	cost = 100,
	texture_id = "twitch_icon_heavens_lightning",
	description = Localize("description_mutator_lightning_strike"),
	texture_size = {
		60,
		70
	},
	condition_func = function (current_vote)
		return not Managers.state.game_mode._mutator_handler:has_activated_mutator("lightning_strike") and mod:get("twitch_allow_mutators")
	end,
	on_success = function (is_server)
		if is_server then
			local mutator_handler = Managers.state.game_mode._mutator_handler
			local mutator_name = "lightning_strike"
			local duration = 33 * mod:get("twitch_mutators_duration_multiplier")

			debug_print(string.format("[TWITCH VOTE] Activating mutator %s", mutator_name))
			mutator_handler:initialize_mutators({
				mutator_name
			})
			mutator_handler:activate_mutator(mutator_name, duration, "activated_by_twitch")
			add_buff_to_all_players("twitch_mutator_buff_lightning_strike")
		end
	end
}
mutator_vote_templates.twitch_vote_activate_chasing_spirits = {
	text = Localize("display_name_chasing_spirits"),
	cost = 100,
	texture_id = "twitch_icon_death_spirits",
	description = Localize("description_mutator_chasing_spirits"),
	texture_size = {
		60,
		70
	},
	condition_func = function (current_vote)
		return not Managers.state.game_mode._mutator_handler:has_activated_mutator("chasing_spirits") and mod:get("twitch_allow_mutators")
	end,
	on_success = function (is_server)
		if is_server then
			local mutator_handler = Managers.state.game_mode._mutator_handler
			local mutator_name = "chasing_spirits"
			local duration = 30 * mod:get("twitch_mutators_duration_multiplier")

			debug_print(string.format("[TWITCH VOTE] Activating mutator %s", mutator_name))
			mutator_handler:initialize_mutators({
				mutator_name
			})
			mutator_handler:activate_mutator(mutator_name, duration, "activated_by_twitch")
			add_buff_to_all_players("twitch_mutator_buff_chasing_spirits")
		end
	end
}
mutator_vote_templates.twitch_vote_activate_flames = {
	text = Localize("display_name_flames"),
	cost = 100,
	texture_id = "twitch_icon_fire_burn",
	description = Localize("description_mutator_flames"),
	texture_size = {
		60,
		70
	},
	condition_func = function (current_vote)
		return not Managers.state.game_mode._mutator_handler:has_activated_mutator("flames") and mod:get("twitch_allow_mutators")
	end,
	on_success = function (is_server)
		if is_server then
			local mutator_handler = Managers.state.game_mode._mutator_handler
			local mutator_name = "flames"
			local duration = 30 * mod:get("twitch_mutators_duration_multiplier")

			debug_print(string.format("[TWITCH VOTE] Activating mutator %s", mutator_name))
			mutator_handler:initialize_mutators({
				mutator_name
			})
			mutator_handler:activate_mutator(mutator_name, duration, "activated_by_twitch")
			add_buff_to_all_players("twitch_mutator_buff_flames")
		end
	end
}

return mutator_vote_templates