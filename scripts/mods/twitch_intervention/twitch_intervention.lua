--[[
  Copyright 2020 Lucas Schwiderski

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
--]]

local mod = get_mod("twitch_intervention")

local Managers = Managers
local TwitchManager = TwitchManager

-- Set up persistent managers table.
-- We don't want to use the functions `default_table` param, we want to use lazy evaluation of `and`
-- to only create new objects when we have to.
local managers = mod:persistent_table("managers")

-- Create managers in order they are needed
dofile("scripts/mods/twitch_intervention/managers/buff_system")
managers.buff_manager = managers.buff_manager or TwitchBuffSystem:new()

dofile("scripts/mods/twitch_intervention/managers/hook_extender")
managers.hook_extender = managers.hook_extender or HookExtender:new()

dofile("scripts/mods/twitch_intervention/managers/template_manager")
managers.template_manager = managers.template_manager or TwitchTemplateManager:new()

dofile("scripts/mods/twitch_intervention/managers/event_emitter")
dofile("scripts/mods/twitch_intervention/managers/game_modes/base_game_mode")
dofile("scripts/mods/twitch_intervention/managers/game_modes/vanilla_game_mode")
dofile("scripts/mods/twitch_intervention/managers/game_modes/random_game_mode")
dofile("scripts/mods/twitch_intervention/managers/twitch_manager")
managers.twitch_manager = managers.twitch_manager or TwitchInterventionManager:new()


-- Keep root level references for efficiency
local buff_manager = managers.buff_manager
local template_manager = managers.template_manager
local twitch_manager = managers.twitch_manager

dofile("scripts/mods/twitch_intervention/debugger")

local ingame_hud_definitions = require("scripts/ui/views/ingame_hud_definitions")
local visibility_groups_lookup = ingame_hud_definitions.visibility_groups_lookup

-- UNOFFICIAL PATCHES

-- Seemingly random units can become invincible once a "Reinforcement" vote template was executed
-- In 1.2 FS introduced the idea of reusing units and their associated data, such as extensions.
-- For this, they implemented functionality to "freeze" an extension
-- (the state when the unit would have been destroyed and was moved to the cache)
-- and to reset it. Sadly, they did not implemented this thoroughly and missed some values that needed to be reset,
-- in particular `GenericHealthExtension.is_invincible`.
-- When a unit was spawned from a "Reinforcement" vote, it would have that flag set. So when that unit was moved to cache and reset
-- to be used by another unit later on, it would keep the invincible state making the new unit invincible as well.
-- That one however would not be tracked by a limited-time-spawn-vote and would stay alive forever.
mod:hook_safe(GenericHealthExtension, "reset", function(self)
  self.is_invincible = nil
end)

-- However, even if `self.is_invincible` is reset correctly, units will still be seemingly invincible,
-- due to the way invincibility is initialized in the first place
-- There is no dedicated parameter to make a unit invincible and instead a initial health value of `-1` is used.
-- Because of that the parameters do not contain the actual health value so after a reset, the huge value it was set to remains.
mod:hook_origin(GenericHealthExtension, "init", function(self, extension_init_context, unit, extension_init_data)
  self.unit = unit
	self.is_server = Managers.player.is_server
	self.system_data = extension_init_context.system_data
	self.statistics_db = extension_init_context.statistics_db
	self.damage_buffers = {
		pdArray.new(),
		pdArray.new()
	}
  self.network_transmit = extension_init_context.network_transmit

  local health = extension_init_data.health or Unit.get_data(unit, "health")
  self.is_invincible = extension_init_data.invincible or false
	self.dead = false
	self.predicted_dead = false
	self.state = "alive"
	self.damage = extension_init_data.damage or 0
	self.predicted_damage = 0
  self.last_damage_data = {}

	self:set_max_health(health)

  self.unmodified_max_health = self.health
	self._health_clamp_min = nil
	self._recent_damage_type = nil
	self._recent_hit_react_type = nil
  self._damage_cap_per_hit = extension_init_data.damage_cap_per_hit or Unit.get_data(unit, "damage_cap_per_hit") or health
end)

-- UNOFFICIAL PATCHES END

mod:hook_safe(StateIngame, "on_enter", function(self)
  managers.debugger = TwitchInterventionDebugger:new(self.world, self.is_server)
end)

mod:hook_safe(StateIngame, "on_exit", function()
  managers.debugger = nil
end)

-- The mod manager is initialized and mods are loaded before most of the other managers are initialized.
-- So simply setting `Managers.twitch` would just be overwritten shortly afterwards by the vanilla manager
mod:hook_safe(Game, "_init_managers", function()
  if Managers.twitch ~= twitch_manager then
    Managers.twitch = twitch_manager
    mod:echo("Twitch Plus is registered and ready.")
  end
end)

-- Additional hooks
dofile("scripts/mods/twitch_intervention/hooks/connecting")
dofile("scripts/mods/twitch_intervention/hooks/twitch_vote_ui")

-- Chat comands
dofile("scripts/mods/twitch_intervention/commands/twitch")
dofile("scripts/mods/twitch_intervention/commands/voting")

function mod.update(dt)
  if not mod:is_enabled() then
    return
  end

  buff_manager:update(dt)

  if managers.debugger and mod:get("debug_enabled") then
    managers.debugger:update(dt)
  end
end

mod.on_game_state_changed = function(status, state_name)
  buff_manager:on_game_state_changed(status, state_name)
end

function mod.on_enabled()
  if Managers.twitch ~= twitch_manager then
    Managers.twitch = twitch_manager
    mod:echo("Twitch Plus is registered and ready.")
  end

  LevelSettings.plaza.disable_twitch_game_mode = not mod:get("twitch_enable_plaza")
  visibility_groups_lookup["dead"].visible_components["TwitchVoteUI"] = true
end

-- When unloading or disabling the mod, reset to vanilla twitch mode
local function reset_to_vanilla_manager()
  twitch_manager:reset()
  Managers.twitch = TwitchManager:new()
end

function mod.on_disabled()
  reset_to_vanilla_manager()

  LevelSettings.plaza.disable_twitch_game_mode = true
  visibility_groups_lookup["dead"].visible_components["TwitchVoteUI"] = true
end

function mod.on_unload()
  reset_to_vanilla_manager()

  -- Destroy TemplateManager and BuffSystem completely, so that new templates and buffs are applied on reload
  template_manager:delete()
  template_manager = nil
  managers.template_manager = nil

  buff_manager:delete()
  buff_manager = nil
  managers.buff_manager = nil
end

function mod.on_setting_changed(setting_name)
  if not mod:is_enabled() then
    return
  end

  if setting_name == "twitch_enable_plaza" then
    LevelSettings.plaza.disable_twitch_game_mode = not mod:get("twitch_enable_plaza")
  end
end

--[[
  Mod API
--]]

--[[
add_template adds the provided vote template to the pool of templates available to the twitch integration
--]]
mod.add_template = function(name, template)
  return template_manager:add(name, template)
end

local function add_templates_to_manager(templates)
  for name, template in pairs(templates) do
    template_manager:add(name, template)
  end
end

add_templates_to_manager(dofile("scripts/mods/twitch_intervention/vote_templates/buffs"))
add_templates_to_manager(dofile("scripts/mods/twitch_intervention/vote_templates/items"))
add_templates_to_manager(dofile("scripts/mods/twitch_intervention/vote_templates/spawning"))
add_templates_to_manager(dofile("scripts/mods/twitch_intervention/vote_templates/mutators"))
