--[[
  Copyright 2020 Lucas Schwiderski

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
--]]

return {
  mod_description = {
    en = "A overhaul of the twitch integration adding lots of customizability and new stuff.",
  },
  vote_time_options_group_name = {
    en = "Vote Time",
  },
  option_twitch_time_between_votes_name = {
    en = "Time Between Votes",
  },
  option_twitch_time_between_votes_tooltip = {
    en = "Duration in seconds between each twitch vote.",
  },
  option_twitch_vote_time_name = {
    en = "Vote Duration",
  },
  option_twitch_vote_time_tooltip = {
    en = "Duration in seconds for which viewers can vote.",
  },
  option_twitch_initial_downtime_name = {
    en = "Initial Downtime",
  },
  option_twitch_initial_downtime_tooltip = {
    en = "Duration in seconds before the first vote starts after begin of the mission.",
  },
  option_randomize_template_outcome_name = {
    en = "Randomize Spawn Events"
  },
  option_randomize_template_outcome_tooltip = {
    en = "Randomize the outcome of voting events.\nEnemies will spawn in slightly different numbers each time, within ranges specific to each vote.\nSimilar semantics apply to buff durations."
  },
  option_randomize_template_multiplier_name = {
    en = "Additional multiplier applied to the upper bound of the random range."
  },
  option_randomize_template_multiplier_tooltip = {
    en = "Multiplier to apply on top of the randmized numbers."
  },
  option_randomize_boss_template_outcome_name = {
    en = "Include Bosses"
  },
  option_exclude_bot_players_name = {
    en = "Exclude Bot Players"
  },
  option_exclude_bot_players_tooltip = {
    en = "Votes for bots will be ignored when viewers choose the target player."
  },
  option_exclude_dead_players_name = {
    en = "Exclude Dead Players"
  },
  option_exclude_dead_players_tooltip = {
    en = "Votes for dead players will be ignored when viewers choose the target player."
  },

  -- NEW
  option_twitch_game_mode_name = {
    en = "Game Mode",
  },
  option_twitch_game_mode_tooltip = {
    en = "Game Mode to activate. The game mode determines how votes are chosen.\n\n"..
      "Default: The default behaviour as implemented by Fatshark attempts to even out 'good' vs. 'bad' votes.\n"..
      "Random: Choses votes completely random and unbiased. Yes, you might get three bosses in a row.",
  },
  option_twitch_game_mode_option_vanilla = {
    en = "Default",
  },
  option_twitch_game_mode_option_random = {
    en = "Random",
  },
  option_twitch_enable_vote_sounds_name = {
    en = "Enable UI Sounds",
  },
  option_twitch_enable_vote_sounds_tooltip = {
    en = "Play sounds when a new vote starts and ends.",
  },
  option_twitch_allow_multiple_votes_name = {
    en = "Allow Multiple Votes",
  },
  option_twitch_allow_multiple_votes_tooltip = {
    en = "Allow multiple votes per user.",
  },
  option_debug_enabled_name = {
    en = "Show Debug UI",
  },
  option_twitch_enable_plaza_name = {
    en = "Enable on Fortunes of War",
  },
  option_twitch_enable_plaza_tooltip = {
    en = "Enable Twitch integration on Fortunes of War.",
  },
  option_auto_fill_username_name = {
    en = "Auto-fill Twitch Username",
  },
  option_auto_fill_username_tooltip = {
    en = "Automatically fills the username field for Twitch Integration with the most recently used value.",
  },
  option_auto_connect_name = {
    en = "Auto-connect",
  },
  option_auto_connect_tooltip = {
    en = "Automatically connect to Twitch after booting the game.",
  },
  option_twitch_allow_mutators_name = {
    en = "Enable Mutator Votes",
  },
  option_twitch_allow_mutators_tooltip = {
    en = "Allow votes that enable mutators.",
  },
  option_twitch_mutators_duration_multiplier_name = {
    en = "Mutator Vote Duration Multiplier",
  },
  option_twitch_mutators_duration_multiplier_tooltip = {
    en = "This multiplier is applied to the duration of mutator.",
  },
  option_twitch_allow_positive_votes_name = {
    en = "Enable Positive Votes",
  },
  option_twitch_allow_positive_votes_tooltip = {
    en = "Allow votes that help the party, such as giving items or buffs.",
  },
  option_twitch_allow_limited_time_votes_name = {
    en = "Enable Reinforcement Votes",
  },
  option_twitch_allow_limited_time_votes_tooltip = {
    en = "Allow votes that spawn reinforcement enemies. Reinforcement enemies cannot be killed but will disappear after a short while.",
  },

  -------------------------------------------------------
  -- Vote Template Names
  -------------------------------------------------------

  twitch_vote_spawn_lit_explosive_barrels_for_players = {
    en = "We'll blow you up!",
  },
  twitch_vote_spawn_lit_explosive_barrels = {
    en = "Fire in the hole!",
  },
  twitch_vote_spawn_lit_fire_barrels = {
    en = "The floor is lava",
  },
  twitch_vote_spawn_berzerkers = {
    en = "What A Savage"
  },
  twitch_vote_spawn_plague_monks = {
    en = "Clan Pestilens"
  },
  twitch_vote_spawn_backstabbers = {
    en = "Watch My Back"
  },
  twitch_vote_one_punch_man = {
    en = "Konnichiwa, Saitama-san!"
  },
  twitch_vote_change_wielded_weapon = {
    en = "A new one every day"
  },
  twitch_vote_no_sound = {
    en = "Silence is gold"
  },
  twitch_vote_randomize_input = {
    en = "What does this button do?"
  },
  twitch_vote_swap_player_positions = {
    en = "Where am I?"
  },
  twitch_vote_spawn_all_four_mini_bosses = {
    en = "The Fated Four... or 5, doens't matter"
  },
  twitch_vote_increased_friendly_fire = {
    en = "FF \"Tweaks\""
  },
  twitch_vote_no_ult_cooldown = {
    en = "Press 'F' to pay respects"
  },
  twitch_vote_maximum_crit_chance = {
    en = "Critical Hit!"
  },
  twitch_vote_companion_rat = {
    en = "Little brat"
  },
  twitch_vote_increased_dodge_range = {
    en = "Dodged"
  },
  twitch_vote_increased_move_speed = {
    en = "Run, dense collection of trees, Run"
  },
  twitch_vote_increased_attack_speed = {
    en = "Swish, Swoosh, Swish, Swish"
  },
  twitch_vote_mini_rat_ogre = {
    en = "roJR"
  },
  twitch_vote_limited_time_ogres = {
    en = "Reinforcements: Rat Ogre"
  },
  twitch_vote_limited_time_chaos_spawns = {
    en = "Reinforcements: Chaos Spawn"
  },
  twitch_vote_limited_time_trolls = {
    en = "Reinforcements: Chaos Troll"
  },
  twitch_vote_limited_time_stormfiends = {
    en = "Reinforcements: Stormfiend"
  },
  twitch_vote_limited_time_skaven_squad = {
    en = "Reinforcements: Skaven Squad"
  },
  twitch_vote_limited_time_skaven_patrol = {
    en = "Reinforcements: Skaven Patrol"
  },
  twitch_vote_limited_time_chaos_patrol = {
    en = "Reinforcements: Chaos Patrol"
  },
  twitch_vote_limited_time_chaos_squad = {
    en = "Reinforcements: Chaos Squad"
  },
  twitch_vote_increased_jump = {
    en = "I ain't afraid of heights"
  },

  -------------------------------------------------------
  -- Command Dscription
  -------------------------------------------------------

  command_twitch_description = {
    en = "Debug commands for the twitch mode"
  },
  command_vote_a_description = {
    en = "Vote for option A"
  },
  command_vote_b_description = {
    en = "Vote for option B"
  },
  command_vote_c_description = {
    en = "Vote for option C"
  },
  command_vote_d_description = {
    en = "Vote for option D"
  },

  -------------------------------------------------------
  -- Other
  -------------------------------------------------------
  connection_failed_error_message = {
    en = "Failed to connect.",
  },
}
